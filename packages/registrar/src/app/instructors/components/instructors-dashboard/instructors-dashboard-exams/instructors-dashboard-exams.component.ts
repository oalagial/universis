import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as INSTRUCTORS_EXAMS_LIST_CONFIG from './instructors-dashboard-exams.config.list.json';
import { Subscription } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { ActivatedTableService } from '../../../../tables/tables.activated-table.service';
import { AdvancedSearchFormComponent } from 'packages/registrar/src/app/tables/components/advanced-search-form/advanced-search-form.component';


@Component({
  selector: 'app-instructors-dashboard-exams',
  templateUrl: './instructors-dashboard-exams.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardExamsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>INSTRUCTORS_EXAMS_LIST_CONFIG;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  private dataSubscription: Subscription;
  instructorID: any = this._activatedRoute.snapshot.params.id;
  @ViewChild('exams') exams: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _translateService: TranslateService,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _context: AngularDataContext) { }

  async ngOnInit() {

    this._activatedTable.activeTable = this.exams;

    this.exams.query = this._context.model('courseExamInstructors')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseExam($expand=course,examPeriod)')
      .prepare();

    this.exams.config = AdvancedTableConfiguration.cast(INSTRUCTORS_EXAMS_LIST_CONFIG);

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.exams.fetch(true);
      }
    });

    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      if (data.tableConfiguration) {
        this.exams.config = data.tableConfiguration;
        this.exams.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.exams && this.exams.selected && this.exams.selected.length) {
      const items = this.exams.selected.map(item => {
        return {
          instructor: this.instructorID,
          courseExam: item.id
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Instructors.RemoveExamsTitle'),
        this._translateService.instant('Instructors.RemoveExamMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('CourseExamInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Instructors.RemoveExamsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Instructors.RemoveExamsMessage.one' : 'Instructors.RemoveExamsMessage.many')
                  , { value: items.length })
              );
              this.exams.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

    }
  }

}

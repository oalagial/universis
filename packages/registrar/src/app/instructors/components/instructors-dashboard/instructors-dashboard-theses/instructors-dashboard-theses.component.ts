import {AfterViewInit, Component, ElementRef, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
declare var $;


@Component({
  selector: 'app-instructors-dashboard-theses',
  templateUrl: './instructors-dashboard-theses.component.html',
  styleUrls: ['../instructors-dashboard.component.scss']
})
export class InstructorsDashboardThesesComponent implements OnInit {
  public model: any;



  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Theses')
      .where('instructor').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person))')
      .getItems();
  }
}

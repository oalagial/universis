import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InstructorsDashboardOverviewSupervisedComponent } from './instructors-dashboard-overview-supervised.component';

describe('InstructorsDashboardOverviewSupervisedComponent', () => {
  let component: InstructorsDashboardOverviewSupervisedComponent;
  let fixture: ComponentFixture<InstructorsDashboardOverviewSupervisedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InstructorsDashboardOverviewSupervisedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InstructorsDashboardOverviewSupervisedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

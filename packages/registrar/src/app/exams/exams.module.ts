import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExamsHomeComponent } from './components/exams-home/exams-home.component';
import { ExamsRoutingModule } from './exams.routing';
import { ExamsSharedModule } from './exams.shared';
import { ExamsTableComponent } from './components/exams-table/exams-table.component';
import { ExamsPreviewComponent } from './components/exams-preview/exams-preview.component';
import { ExamsRootComponent } from './components/exams-root/exams-root.component';
import { TablesModule } from '../tables/tables.module';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { ExamsPreviewGeneralComponent } from './components/exams-preview-general/exams-preview-general.component';
import { ExamsPreviewStudentsComponent } from './components/exams-preview-students/exams-preview-students.component';
import { ExamsPreviewInstructorsComponent } from './components/exams-preview-instructors/exams-preview-instructors.component';
import { StudentsSharedModule } from './../students/students.shared';
import { InstructorsSharedModule } from './../instructors/instructors.shared';
import { ElementsModule } from '../elements/elements.module';
import { ExamsPreviewGradingComponent } from './components/exams-preview-grading/exams-preview-grading.component';
import {UploadsPreviewFormComponent} from './components/exams-preview-grading/uploads-preview-form.component';
import { ExamsPreviewGradesCheckComponent } from './components/exams-preview-grades-check/exams-preview-grades-check.component';
import {ExamsAddInstructorComponent} from './components/exams-preview-instructors/exams-add-instructor.component';
import {MostModule} from '@themost/angular';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { AdvancedFormsModule } from '@universis/forms/lib/advanced-forms.module';
import { RouterModalModule } from 'packages/common/routing/src/RouterModalModule';
import {TooltipModule} from 'ngx-bootstrap';
import {ChartsModule} from 'ng2-charts';
import { ExamsPreviewGeneralStatisticsComponent } from './components/exams-preview-general/exams-preview-general-statistics/exams-preview-general-statistics.component';
import { CourseExamUploadActionsComponent } from './components/exams-preview-general/course-exam-upload-actions/course-exam-upload-actions.component';
import { ExamsPreviewGeneralExamInfoComponent } from './components/exams-preview-general/exams-preview-general-exam-info/exams-preview-general-exam-info.component';
import { ExamsPreviewGeneralInstructorsComponent } from './components/exams-preview-general/exams-preview-general-instructors/exams-preview-general-instructors.component';
import { CoursesSharedModule } from '../courses/courses.shared';
import {ExamsPreviewTestTypesComponent} from './components/exams-preview-test-types/exams-preview-test-types.component';
import {ExamsAddTestTypeComponent} from './components/exams-preview-test-types/exams-add-test-type.component';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    ExamsSharedModule,
    TablesModule,
    ExamsRoutingModule,
    FormsModule,
    SharedModule,
    StudentsSharedModule,
    InstructorsSharedModule,
    ElementsModule,
    MostModule,
    BsDatepickerModule.forRoot(),
    AdvancedFormsModule,
    RegistrarSharedModule,
    RouterModalModule,
    TooltipModule.forRoot(),
    ChartsModule,
    CoursesSharedModule,
    ReportsSharedModule
  ],
  providers: [DatePipe],
  declarations: [
    ExamsHomeComponent,
    ExamsTableComponent,
    ExamsPreviewComponent,
    ExamsRootComponent,
    ExamsPreviewGeneralComponent,
    ExamsPreviewStudentsComponent,
    ExamsPreviewInstructorsComponent,
    ExamsPreviewGradingComponent,
    UploadsPreviewFormComponent,
    ExamsPreviewGradesCheckComponent,
    ExamsAddInstructorComponent,
    ExamsPreviewGeneralStatisticsComponent,
    CourseExamUploadActionsComponent,
    ExamsPreviewGeneralExamInfoComponent,
    ExamsPreviewGeneralInstructorsComponent,
    ExamsPreviewTestTypesComponent,
    ExamsAddTestTypeComponent
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ExamsModule { }

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService, ErrorService, LoadingService} from '@universis/common';
import {ApplicationSettings} from '../../../registrar-shared/registrar-shared.module';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-grades-check',
  templateUrl: './exams-preview-grades-check.component.html',
  styleUrls: ['./exams-preview-grades-check.component.scss']
})
export class ExamsPreviewGradesCheckComponent implements OnInit, OnDestroy  {

  public courseexamId;
  public courseExamAction: any;
  public instructor: any;
  public useDigitalSignature = true;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService,
              private errorService: ErrorService,
              private _config: ConfigurationService

  ) { }

  async ngOnInit() {
    try {
      this._loadingService.showLoading();
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        // get digital signature settings from app config file
        this.useDigitalSignature = !!(<ApplicationSettings>this._config.settings.app).useDigitalSignature;

        this.courseexamId = this._activatedRoute.snapshot.parent.params.id;
        const gradeId = params.id;
        this.courseExamAction = await this._context.model(`CourseExams/${this.courseexamId}/actions`)
          .asQueryable()
          .expand('additionalResult,attachments,grades,owner,object($expand=examPeriod,' +
            'course($expand=department($expand=currentPeriod,currentYear))' +
            ',classes($expand=courseClass($expand=period,year)),examPeriod,year)')
          .where('id').equal(gradeId)
          .getItem();
        if (this.courseExamAction.owner) {
          this.instructor = await this._context.model('Instructors')
            .where('user').equal(this.courseExamAction.owner.id)
            .getItem();
        }
      });
      this._loadingService.hideLoading();
    } catch (err) {
      this._loadingService.hideLoading();
      return this.errorService.navigateToError(err);
    }

  }

  attachedGrades(attachments) {
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachments[0].url.replace(/\\/g, '/').replace('/api', '');
    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachments[0].name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

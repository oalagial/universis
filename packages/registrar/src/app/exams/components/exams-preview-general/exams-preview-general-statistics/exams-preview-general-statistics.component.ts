import {Component, OnDestroy, OnInit} from '@angular/core';
import { ChartOptions } from 'chart.js';
import { GradeScale, GradeScaleService } from '@universis/common';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-general-statistics',
  templateUrl: './exams-preview-general-statistics.component.html',
  styleUrls: ['./exams-preview-general-statistics.component.scss']
})
export class ExamsPreviewGeneralStatisticsComponent implements OnInit, OnDestroy  {

  public courseExam: any;
  public uploadGrades: any;
  public active: number;
  public statistics: any;
  public studentsRegisters = 0;
  public studentsGraded = 0;
  public studentsSuccessed = 0;
  public defaultGradeScale: GradeScale;
  public isLoading = true;
  private subscription: Subscription;

  // Doughnut
  public doughnutChartLabels;
  public doughnutChartData;
  public doughnutChartType = 'doughnut';
  public doughnutOptions: ChartOptions = {
    responsive: true,
    legend: {
      display: true,
      position: 'bottom'
    }
  };

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private gradeScaleService: GradeScaleService ) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseExam = await this._context.model('CourseExams')
        .where('id').equal(params.id)
        .expand('course($expand=gradeScale),examPeriod,status,completedByUser,year')
        .getItem();

      this.statistics = await this._context.model('StudentGrades').select('examGrade as examGrade', 'isPassed', 'count(id) as total')
        .where('courseExam').equal(params.id)
        .and('status/alternateName').notEqual('pending')
        .groupBy('examGrade', 'isPassed')
        .getItems();
      this.loadStatistics();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  loadStatistics() {
    this.isLoading = true;
    const data = this.statistics;
    this.studentsRegisters = data
      // map count
      .map(x => x.total)
      // calculate sum
      .reduce((a, b) => a + b, 0);
    this.studentsRegisters = data
      // map count
      .map(x => x.total)
      // calculate sum
      .reduce((a, b) => a + b, 0);
    // get students with grade
    this.studentsGraded = data
      // filter students by grade
      .filter(x => {
        return x.examGrade != null;
      })
      // map count
      .map(x => x.total)
      // calculate sum
      .reduce((a, b) => a + b, 0);

    // get students passed
    this.studentsSuccessed = data
      // filter passed grades
      .filter(x => {
        return x.isPassed;
      })
      // map count
      .map(x => x.total)
      // calculate sum
      .reduce((a, b) => a + b, 0);
    // get grade scale values
    let gradeScaleValues = [];
    // const gradeScale  = this.courseExam.course.gradeScale;
    this.gradeScaleService.getGradeScale(this.courseExam.course.gradeScale.id).then(gradeScale => {

      // numeric grade scale
      if (gradeScale.scaleType === 0) {
        // get grade scale min
        const gradeScaleMinValue = parseInt(gradeScale.format(0), 10);
        // get grade scale max
        const gradeScaleMaxValue = parseInt(gradeScale.format(1), 10);
        for (let grade = gradeScaleMinValue; grade < gradeScaleMaxValue; grade++) {
          gradeScaleValues.push({
            valueFrom: gradeScale.convert(grade),
            valueTo: gradeScale.convert(grade + 1),
            name: `${grade}-${grade + 1}`,
            total: 0
          });
        }
      } else {
        gradeScaleValues = gradeScale['values'].map(value => {
          return {
            valueFrom: value.valueFrom,
            valueTo: value.valueTo,
            name: value.name,
            total: 0
          };
        });
      }
      // get total students per value
      gradeScaleValues.forEach(value => {
        value.total = data.filter(x => (x.examGrade === 1 && x.examGrade >= value.valueFrom && x.examGrade <= value.valueTo) ||
          (x.examGrade >= value.valueFrom && x.examGrade < value.valueTo))
          .map(x => x.total)
          .reduce((a, b) => a + b, 0);
      });
      // get chart data
      const _chartGradesData: any = gradeScaleValues.map(x => x.total);
      const _chartGradesLabels: any = gradeScaleValues.map(x => x.name);

      this.doughnutChartLabels = _chartGradesLabels;
      this.doughnutChartData = [_chartGradesData];
      this.isLoading = false;
    });
  }

}

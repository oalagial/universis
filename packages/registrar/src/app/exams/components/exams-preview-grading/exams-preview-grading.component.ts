import {Component, ElementRef, Input, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-exams-preview-grading',
  templateUrl: './exams-preview-grading.component.html',
})
export class ExamsPreviewGradingComponent implements OnInit, OnDestroy  {
  public model: any;
  public active: number;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _translate: TranslateService,
              private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _loadingService: LoadingService) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._loadingService.showLoading();
      this.model = await this._context.model('CourseExams/' + params.id + '/actions')
        .asQueryable()
        .expand('owner,additionalResult,grades($select=action,count(id) as totalCount;$groupby=action)')
        .where('additionalResult').notEqual(null)
        .orderByDescending('dateCreated')
        .take(-1)
        .getItems();
      this._loadingService.hideLoading();

      this.active = (this.model || []).filter(x => {
        return x.actionStatus && x.actionStatus.alternateName === 'ActiveActionStatus';
      }).length;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

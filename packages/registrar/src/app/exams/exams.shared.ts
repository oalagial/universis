import { NgModule, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SharedModule } from '@universis/common';
import { FormsModule } from '@angular/forms';
import {StudentsSharedModule} from '../students/students.shared';
import * as DEFAULT_EXAMS_LIST from './components/exams-table/exams-table.config.json';
import { CoursesSharedModule } from '../courses/courses.shared';
import { ExamsDefaultTableConfigurationResolver, ExamsTableSearchResolver, ExamsTableConfigurationResolver } from './components/exams-table/exams-table-config.resolver';
import { ExamsStudentsDefaultTableConfigurationResolver, ExamsStudentsTableSearchResolver, ExamsStudentsTableConfigurationResolver } from './components/exams-preview-students/exams-preview-students-config.resolver';
import { ExamsPreviewFormComponent } from './components/exams-preview-general/exams-preview-form.component';
import * as TEST_TYPES_LIST from './components/exams-preview-test-types/test-types-table.config.json';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        SharedModule,
        FormsModule,
        StudentsSharedModule,
        CoursesSharedModule
    ],
    declarations: [
        ExamsPreviewFormComponent
    ],
    exports: [
        ExamsPreviewFormComponent
    ],
    providers: [
        ExamsDefaultTableConfigurationResolver,
        ExamsTableSearchResolver,
        ExamsTableConfigurationResolver,
        ExamsStudentsDefaultTableConfigurationResolver,
        ExamsStudentsTableSearchResolver,
        ExamsStudentsTableConfigurationResolver
    ]
})
export class ExamsSharedModule implements OnInit {
  public static readonly TestTypesList = TEST_TYPES_LIST;
  public static readonly DefaultExamList = DEFAULT_EXAMS_LIST;
    constructor(private _translateService: TranslateService) {
        this.ngOnInit().catch((err) => {
            console.error('An error occurred while loading exams shared module');
            console.error(err);
        });
    }

    async ngOnInit() {
        environment.languages.forEach((language) => {
            import(`./i18n/exams.${language}.json`).then((translations) => {
                this._translateService.setTranslation(language, translations, true);
            });
        });
    }
}

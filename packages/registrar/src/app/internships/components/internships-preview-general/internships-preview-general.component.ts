import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-internships-preview-general',
  templateUrl: './internships-preview-general.component.html'
})
export class InternshipsPreviewGeneralComponent implements OnInit, OnDestroy {

  @Input() model: any;
  private subscription: Subscription;
  public previousStudentId: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Internships')
        .where('id').equal(params.id)
        // tslint:disable-next-line:max-line-length
        .expand('status, company, internshipPeriod,department,student($expand=person,studyProgram,user,department,inscriptionYear,studentStatus)')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

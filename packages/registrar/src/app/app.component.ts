import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {ConfigurationService} from '@universis/common';
import {ApplicationSettings} from './registrar-shared/registrar-shared.module';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'registrar';

  public constructor(private _titleService: Title, private _config: ConfigurationService) { }

  public setTitle( newTitle: string) {
    this._titleService.setTitle( newTitle );
  }
    
  async ngOnInit() {
    const customTitle = (<ApplicationSettings>this._config.settings.app) && (<ApplicationSettings>this._config.settings.app).title; 

    if (customTitle && customTitle.trim() !== '') {
      this.setTitle(customTitle);
    }
  }
  
}

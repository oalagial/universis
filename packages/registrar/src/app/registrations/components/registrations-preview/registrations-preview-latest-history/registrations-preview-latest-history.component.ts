import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-registrations-preview-latest-history',
  templateUrl: './registrations-preview-latest-history.component.html'
})
export class RegistrationsPreviewLatestHistoryComponent implements OnInit {

  @Input() latestRegistrationHistory: any;
  @Input() lastRegistration: any;



  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

    this.latestRegistrationHistory = await this._context.model('StudentPeriodRegistrations/' + this._activatedRoute.snapshot.params.id)
    .asQueryable()
    .expand('documents($orderby=dateCreated desc;$expand=documentStatus)')
    .getItem();
  }

}

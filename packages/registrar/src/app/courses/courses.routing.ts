import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CoursesHomeComponent} from './components/courses-home/courses-home.component';
import {CoursesTableComponent} from './components/courses-table/courses-table.component';
import {CoursesRootComponent} from './components/courses-root/courses-root.component';
import {CoursesPreviewGeneralComponent} from './components/dashboard/courses-preview-general/courses-preview-general.component';
import {
  CoursesPreviewClassesComponent,
  CourseTitleResolver
} from './components/dashboard/courses-preview-classes/courses-preview-classes.component';
import {CoursesOverviewComponent} from './components/dashboard/courses-overview/courses-overview.component';
import {CoursesExamsComponent} from './components/dashboard/courses-exams/courses-exams.component';
import {CoursesStudyProgramsComponent} from './components/dashboard/courses-study-programs/courses-study-programs.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import {
  AdvancedFormItemResolver,
  AdvancedFormModalComponent,
  AdvancedFormModalData,
  AdvancedFormParentItemResolver
} from '@universis/forms';
import {CoursesTableConfigurationResolver, CoursesTableSearchResolver} from './components/courses-table/courses-table-config.resolver';
import {
  ActiveDepartmentIDResolver, ActiveDepartmentResolver, CurrentAcademicPeriodResolver, CurrentAcademicYearResolver, LastStudyProgramResolver
} from '../registrar-shared/services/activeDepartmentService.service';
// tslint:disable-next-line:max-line-length
import { CoursesExamsSearchResolver } from './components/dashboard/courses-exams/courses-exams-config.resolver';
import {AdvancedListComponent} from '../tables/components/advanced-list/advanced-list.component';
import * as GradeScaleListConfig from './gradeScales.config.list.json';

const routes: Routes = [
  {
    path: 'configuration/GradeScales',
    component: AdvancedListComponent,
    data: {
      model: 'GradeScales',
      tableConfiguration: GradeScaleListConfig,
      description: 'Settings.Lists.GradeScale.Description',
      longDescription: 'Settings.Lists.GradeScale.LongDescription',
      category: 'Settings.Title'
    },
    children: [
      {
        path: 'add',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'new',
          closeOnSubmit: true
        }
      },
      {
        path: ':id/edit',
        pathMatch: 'full',
        component: AdvancedFormModalComponent,
        outlet: 'modal',
        data: <AdvancedFormModalData>{
          action: 'edit',
          closeOnSubmit: true
        },
        resolve: {
          data: AdvancedFormItemResolver
        }
      }
    ]
  },
  {
    path: '',
    component: CoursesHomeComponent,
    data: {
        title: 'Courses'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/activeCourse'
      },
      {
        path: 'list',
        pathMatch: 'full',
        redirectTo: 'list/index'
      },
      {
        path: 'list/:list',
        component: CoursesTableComponent,
        data: {
          title: 'Courses'
        },
        resolve: {
          department: ActiveDepartmentResolver,
          tableConfiguration: CoursesTableConfigurationResolver,
          searchConfiguration: CoursesTableSearchResolver
        },
        children: [
          {
            path: ':id/complete-inscription',
            pathMatch: 'full',
            component: AdvancedFormModalComponent,
            outlet: 'modal',
            data: <AdvancedFormModalData> {
              model: 'Courses',
              action: 'inscription-edit'
            },
            resolve: {
              data: AdvancedFormItemResolver
            }
          }
        ]
      }
    ]
  },
  {
    path: 'create',
    component: CoursesRootComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'new'
      },
      {
        path: 'new',
        component: AdvancedFormRouterComponent,
        data: {
          maxNumberOfRemarking : 4,
          '$state': 1
        },
      }
    ],
    resolve: {
      department: ActiveDepartmentIDResolver
    }
  },
  {
    path: ':id',
    component: CoursesRootComponent,
    data: {
      title: 'Courses Root',
      model: 'Courses'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard'
      },
      {
        path: 'dashboard',
        component: DashboardComponent,
        data: {
          title: 'Courses.Overview'
        },
        children: [
          {
            path: '',
            redirectTo: 'overview',
            pathMatch: 'full'
          },
          {
            path: 'overview',
            component: CoursesOverviewComponent,
            data: {
              title: 'Courses.Overview'
            }
          },
          {
            path: 'general',
            component: CoursesPreviewGeneralComponent,
            data: {
              title: 'Courses.General'
            }
          },
          {
            path: 'classes',
            component: CoursesPreviewClassesComponent,
            data: {
              title: 'Courses.Classes'
            },
            children: [
              {
                path: 'add',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'CourseClasses',
                  action: 'new',
                  title: null,
                  closeOnSubmit: true
                },
                resolve: {
                  course: AdvancedFormParentItemResolver,
                  title: CourseTitleResolver,
                  department: ActiveDepartmentResolver,
                  year: CurrentAcademicYearResolver,
                  period: CurrentAcademicPeriodResolver
                }
              }
            ]
          },
          {
            path: 'exams',
            component: CoursesExamsComponent,
            data: {
              title: 'Courses.Exams'
            },
            resolve: {
              searchConfiguration: CoursesExamsSearchResolver
            },
            children: [
              {
                path: 'exam/new',
                pathMatch: 'full',
                component: AdvancedFormModalComponent,
                outlet: 'modal',
                data: <AdvancedFormModalData> {
                  model: 'CourseExams',
                  closeOnSubmit: true,
                  action: 'new',
                  serviceQueryParams: {
                  }
                },
                resolve: {
                  course: AdvancedFormParentItemResolver,
                  department: ActiveDepartmentResolver,
                  year: CurrentAcademicYearResolver,
                  inscriptionPeriod: CurrentAcademicPeriodResolver
                }
              }
            ]
          },
          {
            path: 'study-programs',
            component: CoursesStudyProgramsComponent,
            data: {
              title: 'Courses.StudyPrograms'
            }
          }
        ]
      },
      {
        path: ':action',
        component: AdvancedFormRouterComponent,
        data: {
          title: 'Courses.Edit'
        }
      }
    ]
  }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class CoursesRoutingModule {

}

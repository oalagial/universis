import {ModuleWithProviders, NgModule, OnInit} from '@angular/core';
import { CommonModule } from '@angular/common';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {SharedModule} from '@universis/common';
import {FormsModule} from '@angular/forms';
import {CoursesPreviewCourseInfoFormComponent} from './components/dashboard/courses-preview-general/courses-preview-course-info-form.component';
import { CoursesOverviewClassesComponent } from './components/dashboard/courses-overview/courses-overview-classes/courses-overview-classes.component';
import { CoursesOverviewStudyProgrammsComponent } from './components/dashboard/courses-overview/courses-overview-study-programms/courses-overview-study-programms.component';
import { CoursesOverviewGeneralComponent } from './components/dashboard/courses-overview/courses-overview-general/courses-overview-general.component';
import {CoursesOverviewExamsComponent} from './components/dashboard/courses-overview/courses-overview-exams/courses-overview-exams.component';
import {
  CoursesTableConfigurationResolver, CoursesTableSearchResolver,
  CoursesDefaultTableConfigurationResolver
} from './components/courses-table/courses-table-config.resolver';
import {CoursesModule} from './courses.module';
import {CoursesOverviewFormComponent} from './components/dashboard/courses-overview/courses-overview-general/courses-overview-form.component';
import {RouterModule} from '@angular/router';
import { CoursesExamsSearchResolver, CoursesExamsConfigurationResolver, CoursesDefaultExamsConfigurationResolver } from './components/dashboard/courses-exams/courses-exams-config.resolver';

import {SelectCourseComponent} from './components/select-course/select-course-component';
import {TablesModule} from '../tables/tables.module';
import {SettingsSharedModule} from '../settings-shared/settings-shared.module';
import {SettingsService} from '../settings-shared/services/settings.service';
import {DepartmentService} from '../departments/services/department-service.service';

@NgModule({
    imports: [
        TranslateModule,
        CommonModule,
        SharedModule,
        FormsModule,
        RouterModule,
        TablesModule,
        SettingsSharedModule
    ],
  declarations: [
    CoursesPreviewCourseInfoFormComponent,
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent,
    CoursesOverviewFormComponent,
    SelectCourseComponent
  ],
    entryComponents: [
    ],
  exports: [
    CoursesPreviewCourseInfoFormComponent,
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent,
    CoursesOverviewFormComponent,
    SelectCourseComponent
  ],
  providers: [
    CoursesOverviewClassesComponent,
    CoursesOverviewStudyProgrammsComponent,
    CoursesOverviewGeneralComponent,
    CoursesOverviewExamsComponent,
    CoursesTableConfigurationResolver,
    CoursesTableSearchResolver,
    CoursesDefaultTableConfigurationResolver,
    CoursesExamsSearchResolver,
    CoursesExamsConfigurationResolver,
    CoursesDefaultExamsConfigurationResolver
  ]
})
export class CoursesSharedModule  implements OnInit {

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoursesSharedModule,
      providers: [
      ],
    };
  }

  constructor(private _translateService: TranslateService, private _settings: SettingsService) {
      this.ngOnInit().catch(err => {
          console.error('An error occurred while loading courses module');
          console.error(err);
      });
  }

  async ngOnInit() {
      // create promises chain
      const sources = environment.languages.map(async (language) => {
          const translations = await import(`./i18n/courses.${language}.json`);
          this._translateService.setTranslation(language, translations, true);
      });
      // execute chain
      await Promise.all(sources);
    // add section for managing grade scales
    // subscribe for language change
    this._translateService.onLangChange.subscribe(() => {
      const Courses = this._translateService.instant('Courses');
      this._settings.addSection({
        name: 'GradeScale',
        description: Courses.Lists.GradeScale.Description,
        longDescription: Courses.Lists.GradeScale.LongDescription,
        category: 'Lists',
        url: `/courses/configuration/GradeScales`
      });
    });
  }

}

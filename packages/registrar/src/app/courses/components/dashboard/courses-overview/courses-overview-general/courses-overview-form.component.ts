import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-overview-form',
  templateUrl: './courses-overview-form.component.html'
})
export class CoursesOverviewFormComponent implements OnInit {

  // courseId is optional, if model is empty
  @Input() courseId: any;
  @Input() model: any;
  @Input() showEdit = true;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    if (!this.model) {
      if (this.courseId) {
        this.model = await this._context.model('Courses')
          .where('id').equal(this.courseId)
          .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
          .getItem();
      }
    }
  }


}

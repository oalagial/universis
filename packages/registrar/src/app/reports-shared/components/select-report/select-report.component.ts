// tslint:disable-next-line:max-line-length
import { Component, OnInit, OnDestroy, Input, ViewChild, TemplateRef, ElementRef, ViewEncapsulation, EventEmitter, Output } from '@angular/core';
import { RouterModalOkCancel } from '@universis/common/routing/public_api';
import { Router, ActivatedRoute } from '@angular/router';
import { AdvancedTableDataResult,
  AdvancedTableConfiguration,
  AdvancedTableComponent } from '../../../tables/components/advanced-table/advanced-table.component';
import { PageInfo } from '../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import * as REPORT_LIST from './select-report.list.config.json';
import { Subscription } from 'rxjs';
import { ReportService, BlobContent } from '../../services/report.service';
import { TranslateService } from '@ngx-translate/core';
import { FormioComponent, FormioRefreshValue } from 'angular-formio';
import { NgxExtendedPdfViewerComponent } from 'ngx-extended-pdf-viewer';
import { AngularDataContext, DataComponent } from '@themost/angular';
import {ErrorService, UserService} from '@universis/common';
import { SignerService } from '../../services/signer.service';
import { HttpClient } from '@angular/common/http';
import { ResponseError } from '@themost/client';
import {blobToBase64String, base64StringToBlob} from 'blob-util';
declare var $: any;

declare interface SelectedReport {
  id: number;
  name: string;
  reportCategory?: any;
  dateCreated?: any;
  dateModified?: any;
  signReport?: boolean;
  url?: string;
  useDocumentNumber?: boolean;
  documentSeries?: any;
}

@Component({
  selector: 'app-select-report',
  templateUrl: './select-report.component.html',
  styleUrls: ['./select-report.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectReportComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  @Input('appliesTo') appliesTo: string;
  @ViewChild('nameTemplate') nameTemplate: ElementRef;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('form') form: FormioComponent;
  @ViewChild('pdfViewer') pdfViewer: NgxExtendedPdfViewerComponent;
  @ViewChild('pdfViewerContainer') pdfViewerContainer: ElementRef<HTMLDivElement>;
  public tableConfig: AdvancedTableConfiguration;
  public searchText: string;
  public loading: boolean;
  public recordsTotal = 0;
  public recordsFiltered = 0;
  public pageInfo = <PageInfo> {
    serverSide: true
  };
  public selectedReport: SelectedReport;
  private selectedItemsSubscription: Subscription;
  private dataSubscription: Subscription;
  private statusSubscription: Subscription;
  public reportForm: any;
  @Output() refreshForm: EventEmitter<FormioRefreshValue> = new EventEmitter<FormioRefreshValue>();
  public blob: any;
  @Input('item') item: any;
  public lastError: any;
  public formData: any = {};
  public signatures: File[] = [];

  constructor(router: Router,
      activatedRoute: ActivatedRoute,
      private _reports: ReportService,
      private _signer: SignerService,
      private _context: AngularDataContext,
      private _translateService: TranslateService,
      private _errorService: ErrorService,
      private _http: HttpClient,
      private _router: Router,
      private _userService: UserService) {
    super(router, activatedRoute);
    // set modal size
    this.modalClass = 'modal-lg';
    // set modal title
    this.modalTitle = 'Reports.Print';
    // disable ok button
    this.okButtonDisabled = true;
    // set button text
    this.okButtonText = 'Reports.NextButton';
  }
  ngOnDestroy(): void {
    if (this.selectedItemsSubscription) {
      this.selectedItemsSubscription.unsubscribe();
    }
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.statusSubscription) {
      this.statusSubscription.unsubscribe();
    }
    this._signer.destroy();
  }

  ngOnInit() {
    const columnTemplate = `
        <span class='d-flex p-2'><i class='fa text-indigo fa-file fa-3x'></i>
          <div class='ml-3 justify-content-center'>
            <div class="name">$\{name\}</div>
            <div class='text-gray-500 details'>$\{reportCategoryName\}</div>
          </div>
          <div class='ml-auto align-self-center'>
            <i class="fas fa-2x text-white fa-check-circle"></i>
          </div>
        </span>
    `;
    const tableConfig = AdvancedTableConfiguration.cast(REPORT_LIST);
    const findColumn = tableConfig.columns.find( column => {
      return column.name === 'name';
    });
    if (findColumn) {
      findColumn.formatString = columnTemplate;
    }
     this.selectedItemsSubscription = this.table.selectedItems.subscribe( (selectedItems: Array<any>) => {
        this.okButtonDisabled = selectedItems.length === 0;
    });
    this.okButtonDisabled = true;
    // get data
    this.dataSubscription = this.activatedRoute.data.subscribe( (data: any) => {
        try {
          // get model
          const model = data.model;
          // validate model
          if (model == null) {
            throw new Error('Current model cannot be empty at this context');
          }
          // get metadata
          return this._context.getMetadata().then( schema => {
            // get entity type
            const entitySet = schema.EntityContainer.EntitySet.find( x => {
                return x.Name === model;
            });
            // entity type cannot be null
            // todo: show message
            if (entitySet == null) {
              // set empty query
              this.table.query = this._context.model('ReportTemplates')
                  .where('id').equal(null)
                  .prepare();
            } else {
              // set query based on current  entity type
              this.table.query = this._context.model('ReportTemplates')
                  .where('reportCategory/appliesTo').equal(entitySet.EntityType)
                  .prepare();
            }
            // get item assigned to this operation
            this.item = data.item;
            // finally set table configuration
            this.table.config = tableConfig;
            // fetch data
            this.table.fetch();
          }).catch (err => {
            console.error(err);
            this._errorService.showError(err, {
              continueLink: '.'
            });
            this.close();
          });
        } catch (err) {
          console.error(err);
          this.close().then(() => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
        }
    });
    // finally try to load signature graphic
    this.tryToGetSignatureGraphic();
  }

  onReportFormLoad() {
    if (this.form == null) {
      return;
    }
    const currentLang = this._translateService.currentLang;
    const translation = this._translateService.instant('Forms');
    if (this.reportForm.settings && this.reportForm.settings.i18n) {
      // try to get local translations
      if (Object.prototype.hasOwnProperty.call(this.reportForm.settings.i18n, currentLang)) {
        // assign translations
        Object.assign(translation, this.reportForm.settings.i18n[currentLang]);
      }
    }
    this.form.formio.i18next.options.resources[currentLang] = { translation : translation };
    this.form.formio.language = currentLang;
    this.form.formio.data = this.selectedReport;
  }

  onRowSelect(row: any) {
    this.table.toggleSelectRow(row);
  }

  onChange(event: any) {
    // handle changes (check if event has isValid property)
    if (Object.prototype.hasOwnProperty.call(event, 'isValid')) {
      // enable or disable button based on form status
      this.okButtonDisabled = !event.isValid;
    }
    if (event.srcElement && event.srcElement.name === 'data[signReport]') {
      if (this.form.formio.submission.data.signReport) {
        // do some extra things
      }
    }
  }

  onKeyUp(event) {
    if (event.keyCode === 13) {
      this.okButtonDisabled = true;
      this.table.search(this.searchText);
    }
  }

  /**
   * A helper function for finding a component by key name
   */
  findComponent(components: Array<any>, key: any) {
    for (let index = 0; index < components.length; index++) {
      const component = components[index];
      if (component.key === key) {
        return component;
      }
      if (component.columns) {
        for (let j = 0; j < component.columns.length; j++) {
          const column = component.columns[j];
          if (Array.isArray(column.components)) {
            const findComponent = this.findComponent(column.components, key);
            if (findComponent) {
              return findComponent;
            }
          }
        }
      }
      if (component.components) {
        const findComponent = this.findComponent(component.components, key);
        if (findComponent) {
          return findComponent;
        }
      }
    }
  }

  /**
   * Get certificate list from signer app
   */
  onCustomEvent(event: any) {
    if (event.type === 'certificates') {
      this.lastError = null;
      // get data
      const data = this.form.submission.data;
      // get form
      const form = this.form.form;
      const authenticate = new Promise<void>((resolve, reject) => {
        if (this._signer.authorization != null) {
          return resolve();
        }
        return this._signer.authenticate({
          username: 'user',
          password: data.typeKeyStorePassword,
          rememberMe: data.keepMeSignedInDuringThisSession
        }).then(() => {
          return resolve();
        }).catch((err) => {
          return reject(err);
        });
      });
      authenticate.then(() => {
        this._signer.getCertificates().then( (certs: Array<any>) => {
          // find component by key
          const selectComponent = this.findComponent(form.components, 'signCertificate');
          if (selectComponent) {
            // map certificate collection
            selectComponent.data.values = certs.map( (cert) => {
              return {
                  label: cert.commonName,
                  value: cert.thumbprint
              };
            });
            if (selectComponent.data.values.length) {
              data.signCertificate = selectComponent.data.values[0].value;
            }
            // refresh form and data
            this.refreshForm.emit({
              form: form,
              submission: {
                data: data
              }
            });
          }
        }).catch( (err) => {
          this.lastError = err;
        });
      }).catch( (err) => {
        this.lastError = err;
      });
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.recordsFiltered = data.recordsFiltered;
    // get first page info (because page is not going to be fired for first page)
    if (data.recordsTotal > 0 && this.pageInfo.recordsTotal === 0) {
      this.pageInfo.recordsTotal = data.recordsTotal;
    }
    const self = this;
  }

  private refreshFormData(data: any) {
    this.refreshForm.emit({
      submission: {
        data: data
      }
    });
    this.form.ngOnChanges({});
  }

  async ok(): Promise<any> {
    if (this.selectedReport == null) {
      // set selected report
      const selected = this.table.selected[0];
      const selectedReport = await this._reports.getReport(selected.id);
      Object.assign(selectedReport, {
        signerServiceStatus: -1
      });
      this.selectedReport = selectedReport;
      // get report form
      const form = await this._reports.getReportFormFor(this.selectedReport);
      // set report form with no data
      this.reportForm = form;
      // set button text
      this.okButtonText = 'Reports.PrintButton';
      // check signer service
      // subscribe for signer status
      this.lastError = null;
      this.statusSubscription = this._signer.queryStatus().subscribe( (serviceStatus) => {
        if (serviceStatus != null) {
          const formData = Object.assign({ }, this.selectedReport, {
            signerServiceStatus: serviceStatus.status
          });
          // if signer service is already authenticated
          if (this._signer.authorization) {
            // get certificates
            return this._signer.getCertificates().then( (certs) => {
              // find select certificate component
              const selectComponent = this.findComponent(form.components, 'signCertificate');
              // and add certificate values
              if (selectComponent) {
                // map certificate collection
                selectComponent.data.values = certs.map( (cert) => {
                  return {
                      label: cert.commonName,
                      value: cert.thumbprint
                  };
                });
              }
              const lastCertificate = this._signer.getLastCertificate();
              if (lastCertificate) {
                // find if last certificate exists
                const findCertificate = certs.find( cert => {
                  return cert.thumbprint = lastCertificate;
                });
                if (findCertificate) {
                  Object.assign(formData, {
                    signCertificate: findCertificate.thumbprint
                  });
                }
              } else if (certs.length) {
                Object.assign(formData, {
                  signCertificate: certs[0].thumbprint
                });
              }
              // refresh report form data
              Object.assign(formData, {
                signerAlreadyAuthenticated: true,
                keepMeSignedInDuringThisSession: true,
                typeKeyStorePassword: '****'
              });
              // refresh form and data
              this.refreshForm.emit({
                form: form,
                submission: {
                  data: formData
                }
              });
            }).catch( (err) => {
              this.lastError = err;
            });
          } else {
            // refresh report form data
            Object.assign(formData, {
              signerAlreadyAuthenticated: false,
              keepMeSignedInDuringThisSession: false,
              typeKeyStorePassword: null
            });
          }
          // refresh form and data
          this.refreshForm.emit({
            form: form,
            submission: {
              data: formData
            }
          });
        }
      });
      return true;
    } else {
      let blob;
      try {
        // print report by passing report parameters
        blob = await this._reports.printReport(this.selectedReport.id, {
          ID: this.item.id,
          REPORT_USE_DOCUMENT_NUMBER: this.selectedReport.useDocumentNumber,
          REPORT_DOCUMENT_SERIES: this.selectedReport.documentSeries
        });
        // if user had selected to sign document
        if (this.selectedReport.signReport) {
          const data = this.form.formio.data;
          // get content location and prepare to sign
          const contentLocation = (<BlobContent>blob).contentLocation;
          // sign document
          blob = await this._signer.signDocument(blob, data.signCertificate, null, this.signatures[0]);
          // replace document
          if (contentLocation) {
            // replace document (add signed)
            await this._signer.replaceDocument({
              url: contentLocation,
              signed: true,
              published: false
            }, blob);
            // and continue
          }
        }
      } catch (err) {
        return this._errorService.showError(err, {
          continueLink: '.'
        });
      }
      // get blob and pass it to pdf viewer
      this.blob = blob;
      this.showViewer();
      return false;
    }
  }
  cancel(): Promise<any> {
    return this.close();
  }

  showViewer() {

    $(this.pdfViewerContainer.nativeElement).show();
    this.pdfViewer.onResize();
    // add close button
    const toolbarViewerRight = $(this.pdfViewerContainer.nativeElement)
      .find('#toolbarViewerRight');
    if (toolbarViewerRight.find('pdf-close-tool').length === 0) {
      const closeText = this._translateService.instant('Reports.Viewer.Close');
      const closeTool = $(`
        <pdf-close-tool>
            <button style="width:auto" class="toolbarButton px-2" type="button">
                ${closeText}
            </button>
        </pdf-close-tool>
      `);
      // prepare to close viewer
      closeTool.find('button').on('click', () => {
        this.selectedReport = null;
        $(this.pdfViewerContainer.nativeElement).hide();
      });
      // insert before first which is the pdf hand tool
      closeTool.insertBefore(toolbarViewerRight.find('pdf-hand-tool'));
    }
  }

  closeViewer() {
    $(this.pdfViewerContainer.nativeElement).hide();
    $('.bd-modal').removeClass('modal-pdf-viewer');
  }

  pdfClose() {
    this._router.navigate(['.'], {
      relativeTo: this.activatedRoute
    });
  }

  pdfPrint() {
    $(this.pdfViewerContainer.nativeElement).find('pdf-print>button').trigger('click');
  }

  pdfDownload() {
    const reportName = <any>this.selectedReport.name;
    this.pdfViewer.filenameForDownload = reportName;
    this.pdfViewer.ngOnChanges({
      filenameForDownload : reportName
    });
    $(this.pdfViewerContainer.nativeElement).find('pdf-download>button').trigger('click');
  }

  onFileSelect(event) {
    this._userService.getUser().then((user) => {
      // get file
      const addedFile = event.addedFiles[0];
      // add file
      this.signatures.push(addedFile);
      // save image to local storage
      blobToBase64String(addedFile).then((value) => {
        localStorage.setItem('user.signatureGraphic', value);
        localStorage.setItem('user.signatureGraphic.type', addedFile.type);
        localStorage.setItem('user.signatureGraphic.name', addedFile.name);
      });
    });
  }

  onFileRemove(event) {
    console.log(event);
    this.signatures.splice(this.signatures.indexOf(event), 1);
    // delete local storage
    localStorage.removeItem('user.signatureGraphic');
    localStorage.removeItem('user.signatureGraphic.type');
    localStorage.removeItem('user.signatureGraphic.name');
  }

  private tryToGetSignatureGraphic() {
    const value = localStorage.getItem('user.signatureGraphic');
    if (value) {
      const type = localStorage.getItem('user.signatureGraphic.type');
      const name = localStorage.getItem('user.signatureGraphic.name');
      const blob = base64StringToBlob(value, type);
      const addFile = new File([blob], name, {type: type});
      this.signatures.push(addFile);
    }
  }


}

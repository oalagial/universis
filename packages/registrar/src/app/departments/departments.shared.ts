import {CUSTOM_ELEMENTS_SCHEMA, NgModule, OnInit, ModuleWithProviders} from '@angular/core';

import { CommonModule } from '@angular/common';
import { environment } from '../../environments/environment';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { SettingsService } from '../settings-shared/services/settings.service';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { DepartmentService } from './services/department-service.service';
import {DepartmentsTableSearchResolver, DepartmentsTDefaultTableConfigurationResolver, DepartmentsTableConfigurationResolver}
from './components/departments-table/departments-table-config.resolver';
import { ArchivedDocumentsConfigurationResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';
import { ArchivedDocumentsConfigurationSearchResolver } from './components/archived-documents-table/archived-documents-table-config.resolver';


@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    TranslateModule,
    SettingsSharedModule
  ],
  declarations: [
  ],
  exports: [
  ],
  providers: [
    DepartmentsTableSearchResolver,
    DepartmentsTDefaultTableConfigurationResolver,
    DepartmentsTableConfigurationResolver,
    ArchivedDocumentsConfigurationResolver,
    ArchivedDocumentsConfigurationSearchResolver
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DepartmentsSharedModule implements OnInit {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DepartmentsSharedModule,
      providers: [
        DepartmentService
      ],
    };
  }

  constructor(
    private _translateService: TranslateService,
    private _settings: SettingsService
  ) {
    this.ngOnInit().catch((err) => {
      console.error('An error occurred while loading DepartmentsSharedModule');
      console.error(err);
    });
  }

  async ngOnInit() {
    const sources = environment.languages.map((language) => {
      return import(`./i18n/departments.${language}.json`)
        .then((translations) => {
          this._translateService.setTranslation(language, translations, true);
        })
        .catch((err) => {
          console.log(err);
        });
    });
    // await for translations
    await Promise.all(sources);
    // add extra sections for managing document series
    // subscribe for language change
    this._translateService.onLangChange.subscribe(() => {
      const Documents = this._translateService.instant('Documents');
      this._settings.addSection({
        name: 'DepartmentDocumentNumberSeries',
        description: Documents.Lists.DepartmentDocumentNumberSeries.Description,
        longDescription:
          Documents.Lists.DepartmentDocumentNumberSeries.LongDescription,
        category: 'Documents',
        url: '/departments/current/documents/series',
      });
      const Settings = this._translateService.instant('Settings');
      this._settings.addSection({
        name: 'Institute',
        description: Settings.Lists.Institute.Description,
        longDescription:
        Settings.Lists.Institute.LongDescription,
        category: 'Institutes',
        url: '/departments/configuration/institutes',
      });
    });
  }
}

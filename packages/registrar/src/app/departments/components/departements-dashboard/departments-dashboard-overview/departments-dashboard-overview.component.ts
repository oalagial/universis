import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-departments-dashboard-overview',
  templateUrl: './departments-dashboard-overview.component.html',
})
export class DepartmentsDashboardOverviewComponent implements OnInit {
  public departments: any;
  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this.departments = await this._context.model('LocalDepartments')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();
  }
}

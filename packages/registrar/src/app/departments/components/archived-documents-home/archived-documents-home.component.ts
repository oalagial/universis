import { Component, OnInit, ViewChild, OnDestroy, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import { Subscription } from 'rxjs';
import { ErrorService } from '@universis/common';
import {AppEventService} from '@universis/common/shared/services/app-event.service';
import pick = require('lodash/pick');
declare var $: any;
import {AdvancedFilterValueProvider} from '../../../tables/components/advanced-table/advanced-filter-value-provider.service';

@Component({
  selector: 'app-archived-documents-home',
  templateUrl: './archived-documents-home.component.html',
  encapsulation: ViewEncapsulation.None
})
export class ArchivedDocumentsHomeComponent implements OnInit, OnDestroy {

  private paramSubscription: Subscription;
  public departmentDocumentNumberSeries: any[];
  @Input() department: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _context: AngularDataContext,
              private _errorService: ErrorService,
              private _router: Router,
              private _appEvent: AppEventService,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider) {
  }

  ngOnInit() {
    this.department = this._activatedRoute.snapshot.data.department.id;

    this.paramSubscription = this._activatedRoute.params.subscribe((params) => {
      this._context.model('DepartmentDocumentNumberSeries')
      .where('department').equal(this.department)
      .orderBy('active desc,id desc')
          .getItems().then((items) => {
        this.departmentDocumentNumberSeries = items;
        this._router.navigateByUrl(`departments/current/documents/series/${this.departmentDocumentNumberSeries[0].id}/items`);
        this._advancedFilterValueProvider.values = {...this._advancedFilterValueProvider.values, 
        "documentSeries": this.departmentDocumentNumberSeries[0].id}
      }).catch((err) => {
        this._errorService.showError(err);
      });
    });

  }


  ngOnDestroy() {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }
  }



import { Component, OnDestroy, OnInit } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-scholarships-preview-general',
  templateUrl: './scholarships-preview-general.component.html'
})
export class ScholarshipsPreviewGeneralComponent implements OnInit, OnDestroy {

  public model: any;
  public studentScholarships: any;
  public scholarshipID;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.scholarshipID = params.id;
      this.model = await this._context.model('Scholarships')
        .where('id').equal(params.id)
        .expand('department', 'organization')
        .getItem();

      this.studentScholarships = await this._context.model('StudentScholarships')
        .where('scholarship').equal(params.id)
        .expand('Student($expand=person,studentStatus,inscriptionYear,inscriptionPeriod)')
        .orderByDescending('grade')
        .getItems();
    });

  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

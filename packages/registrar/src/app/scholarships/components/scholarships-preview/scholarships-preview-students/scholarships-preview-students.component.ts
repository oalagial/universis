import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as SCHOLARSHIPS_STUDENTS_LIST_CONFIG from './scholarships-preview-students.config.json';
import { ActivatedTableService } from '../../../../tables/tables.activated-table.service';
import { Subscription } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-scholarships-preview-students',
  templateUrl: './scholarships-preview-students.component.html'
})
export class ScholarshipsPreviewStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>SCHOLARSHIPS_STUDENTS_LIST_CONFIG;
  @ViewChild('students') students: AdvancedTableComponent;
  scholarshipID: any ;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translateService: TranslateService
  ) { }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this._activatedTable.activeTable = this.students;
      this.scholarshipID = params.id;
      this.students.query = this._context.model('StudentScholarships')
        .where('scholarship').equal(params.id)
        .expand('Student')
        .prepare();
      this.students.config = AdvancedTableConfiguration.cast(SCHOLARSHIPS_STUDENTS_LIST_CONFIG);

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });
    });

  }

  remove() {
    if (this.students && this.students.selected && this.students.selected.length) {
      // get items to remove
      const items = this.students.selected.map(item => {
        return {
          id: item.id,
          student: item.studentID,
          scholarship: this.scholarshipID
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Scholarships.RemoveStudentTitle'),
        this._translateService.instant('Scholarships.RemoveStudentMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('studentScholarships').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Scholarships.RemoveStudentsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Scholarships.RemoveStudentsMessage.one' : 'Scholarships.RemoveStudentsMessage.many')
                  , { value: items.length })
              );
              this.students.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

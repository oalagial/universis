import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ScholarshipsHomeComponent} from './components/scholarships-home/scholarships-home.component';
import {ScholarshipsTableComponent} from './components/scholarships-table/scholarships-table.component';
import {ScholarshipsPreviewComponent} from './components/scholarships-preview/scholarships-preview.component';
import {ScholarshipsRootComponent} from './components/scholarships-root/scholarships-root.component';
import {ScholarshipsPreviewGeneralComponent} from './components/scholarships-preview/scholarships-preview-general/scholarships-preview-general.component';
import {ScholarshipsPreviewStudentsComponent} from './components/scholarships-preview/scholarships-preview-students/scholarships-preview-students.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import {
  ScholarshipsTableConfigurationResolver,
  ScholarshipsTableSearchResolver
} from './components/scholarships-table/scholarships-table-config.resolver';
import {ExamsAddInstructorComponent} from '../exams/components/exams-preview-instructors/exams-add-instructor.component';
import {AdvancedFormItemResolver, AdvancedFormModalData} from '@universis/forms';
import {StudentsSharedModule} from '../students/students.shared';
import {ScholarshipsAddStudentComponent} from './components/scholarships-preview/scholarships-preview-students/scholarships-add-student.component';


const routes: Routes = [
    {
        path: '',
        component: ScholarshipsHomeComponent,
        data: {
            title: 'Scholarships'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'list/index'
            },
            {
              path: 'list',
              pathMatch: 'full',
              redirectTo: 'list/index'
            },
            {
                path: 'list/:list',
                component: ScholarshipsTableComponent,
                data: {
                    title: 'Scholarships'
                },
                resolve: {
                  tableConfiguration: ScholarshipsTableConfigurationResolver,
                  searchConfiguration: ScholarshipsTableSearchResolver
                },
            }
        ]
    },
    {
      path: 'new',
      component: ScholarshipsRootComponent,
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'new'
        },
        {
          path: 'new',
          component: AdvancedFormRouterComponent
        }
      ]
    },
    {
        path: ':id',
        component: ScholarshipsRootComponent,
        data: {
            title: 'Scholarships Home'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'preview'
            },
            {
                path: 'preview',
                component: ScholarshipsPreviewComponent,
                data: {
                    title: 'Scholarships Preview'
                },
                children: [
                    {
                        path: '',
                        redirectTo: 'general'
                    },
                    {
                        path: 'general',
                        component: ScholarshipsPreviewGeneralComponent,
                        data: {
                            title: 'Scholarships Preview General'
                        }
                    },
                    {
                        path: 'students',
                        component: ScholarshipsPreviewStudentsComponent,
                        data: {
                            title: 'Scholarships Preview Students'
                        },
                        children: [
                          {
                            path: 'add',
                            pathMatch: 'full',
                            component: ScholarshipsAddStudentComponent,
                            outlet: 'modal',
                            data: <AdvancedFormModalData> {
                              title: 'Scholarships.AddStudent',
                              config: StudentsSharedModule.StudentsList
                            },
                            resolve: {
                              scholarship: AdvancedFormItemResolver
                            }
                          }
                        ]
                    }
                ]

            },
            {
              path: ':action',
              component: AdvancedFormRouterComponent
            }

        ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class ScholarshipsRoutingModule {
}

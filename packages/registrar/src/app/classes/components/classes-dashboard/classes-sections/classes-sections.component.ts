import {Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
// tslint:disable-next-line:max-line-length
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_SECTIONS_LIST_CONFIG from './classes-sections.config.list.json';
import {DIALOG_BUTTONS, ErrorService, ModalService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {Subscription} from 'rxjs';
import {ActivatedTableService} from '../../../../tables/tables.activated-table.service';
// tslint:disable-next-line:max-line-length
import { AdvancedSearchFormComponent } from 'packages/registrar/src/app/tables/components/advanced-search-form/advanced-search-form.component';





@Component({
  selector: 'app-classes-sections',
  templateUrl: './classes-sections.component.html',
  styleUrls: ['./classes-sections.component.scss']
})
export class ClassesSectionsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_SECTIONS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('sections') sections: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any ;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;


  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext

  ) { }


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.sections;

      this.sections.query = this._context.model('CourseClassSections')
        .where('courseClass').equal(this.courseClassID)
        .expand('instructor1')
        .prepare();

      this.sections.config = AdvancedTableConfiguration.cast(CLASSES_SECTIONS_LIST_CONFIG);
      this.sections.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.sections.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.sections.config = data.tableConfiguration;
          this.sections.ngOnInit();
        }
        /*      if (data.searchConfiguration) {
                this.search.form = data.searchConfiguration;
                this.search.ngOnInit();
              }*/
      });
    });
  }
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    if (this.sections && this.sections.selected && this.sections.selected.length) {
      const items = this.sections.selected.map( item => {
        return {
          courseClass: this.courseClassID,
          id: item.id,
          instructor1: null,
          instructor2: null,
          instructor3: null
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Classes.RemoveSectionTitle'),
        this._translateService.instant('Classes.RemoveSectionMessage'),
        DIALOG_BUTTONS.OkCancel).then( result => {
        if (result === 'ok') {
        return   this._context.model('courseClassSections').save(items).then( () => {
          return this._context.model('courseClassSections').remove(items).then( () => {
            this._toastService.show(
              this._translateService.instant('Classes.RemoveSectionsMessage.title'),
              this._translateService.instant((items.length === 1 ?
                'Classes.RemoveSectionsMessage.one' : 'Classes.RemoveSectionsMessage.many')
                , { value: items.length })
            );
            this.sections.fetch(true);
          }).catch( err => {
            this._errorService.showError(err, {
              continueLink: '.'
            });
          });
          });
        }
      });

    }
  }
}

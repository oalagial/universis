import {Component, Directive, EventEmitter, Input, OnDestroy, OnInit} from '@angular/core';
// tslint:disable-next-line:max-line-length
import {AdvancedTableModalBaseComponent, AdvancedTableModalBaseTemplate} from '../../../../tables/components/advanced-table-modal/advanced-table-modal-base.component';
import {ActivatedRoute, Router} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ErrorService, ToastService} from '@universis/common';
import {TranslateService} from '@ngx-translate/core';
import {AdvancedFilterValueProvider} from './../../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-class-add-instructor',
  template: AdvancedTableModalBaseTemplate
})
export class ClassesAddInstructorComponent extends AdvancedTableModalBaseComponent {

  @Input() courseClass: any;

  constructor(_router: Router, _activatedRoute: ActivatedRoute,
              _context: AngularDataContext,
              private _errorService: ErrorService,
              private _toastService: ToastService,
              private _translateService: TranslateService,
              protected advancedFilterValueProvider: AdvancedFilterValueProvider,
              protected datePipe: DatePipe
              ) {
    super(_router, _activatedRoute, _context, advancedFilterValueProvider, datePipe);
    // set default title
    this.modalTitle = 'Classes.AddInstructor';
  }

  hasInputs(): Array<string> {
    return [ 'courseClass' ];
  }

  ok(): Promise<any> {
    // get selected items
    const selected = this.advancedTable.selected;
    let items = [];
    if (selected && selected.length > 0) {
      // try to add classes
      items = selected.map( instructor => {
        return {
          courseClass: this.courseClass,
          instructor: instructor
        };
      });
      return this._context.model('CourseClassInstructors')
        .save(items)
        .then( result => {
          // add toast message
          this._toastService.show(
            this._translateService.instant('Classes.AddInstructorsMessage.title'),
            this._translateService.instant((items.length === 1 ?
              'Classes.AddInstructorsMessage.one' : 'Classes.AddInstructorsMessage.many')
              , { value: items.length })
          );
          return this.close({
            fragment: 'reload',
            skipLocationChange: true
          });
        }).catch( err => {
          this._errorService.showError(err, {
            continueLink: '.'
          });
        });
    }
    return this.close();
  }
}

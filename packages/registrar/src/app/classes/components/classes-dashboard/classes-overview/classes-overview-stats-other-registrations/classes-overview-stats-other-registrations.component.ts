import {Component, OnDestroy, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-classes-overview-stats-other-registrations',
  templateUrl: './classes-overview-stats-other-registrations.component.html',
  styleUrls: ['./classes-overview-stats-other-registrations.component.scss']
})
export class ClassesOverviewStatsOtherRegistrationsComponent implements OnInit, OnDestroy {

  public class;
  public OtherClassesData;
  private subscription: Subscription;


  public charOtherRegistrationsOptions: any;
  public charOtherRegistrationsLabels: any[] ;
  public charOtherRegistrationsData: any[] ;
  public charOtherRegistrationsType = 'bar';
  public charOtherRegistrationsLegend = true;
  public charOtherRegistrationsColours: Array<any> = [
    { // winter blue
      backgroundColor: '#63c2de',
      borderColor: '#63c2de',
      pointBorderColor: '#63c2de',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#63c2de'
    },
    { // summer red
      backgroundColor: '#f86c6b',
      borderColor: '#f86c6b',
      pointBorderColor: '#f86c6b',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: '#f86c6b'
    }
  ];

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.class = await this._context.model('CourseClasses')
        .where('id').equal(params.id)
        .getItem();

      this.OtherClassesData = await this._context.model('CourseClasses')
        .where('course').equal(this.class.course)
        .expand('period')
        // tslint:disable-next-line:max-line-length
        .select('year/id as yearId,year/alternateName as yearName,numberOfStudents,period/id as periodId, period/alternateName as periodName')
        .getItems();

      this.charOtherRegistrationsOptions = {
        scaleShowVerticalLines: true,
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          position: 'bottom',
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true,
              precision: 0
            },
            scaleLabel: {
              display: true,
              labelString: this._translateService.instant('Classes.NumberOfStudents')
            }
          }]
        }
      };

      this.charOtherRegistrationsLabels = [...new Set(this.OtherClassesData.map(el => el.yearName))];

      this.charOtherRegistrationsData = [
        {data: [], label: this._translateService.instant('Periods.winter')},
        {data: [], label: this._translateService.instant('Periods.summer')}
      ];

      this.charOtherRegistrationsLabels.forEach(year => {
        const winterclass = this.OtherClassesData.find(el => el.periodId === 1 && el.yearName === year);
        this.charOtherRegistrationsData[0].data.push(winterclass ? winterclass.numberOfStudents : 0);

        const summerclass = this.OtherClassesData.find(el => el.periodId === 2 && el.yearName === year);
        this.charOtherRegistrationsData[1].data.push(summerclass ? summerclass.numberOfStudents : 0);
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

import {Component, OnInit, ViewChild, Input, EventEmitter, OnDestroy} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_STUDENTS_LIST_CONFIG from './classes-students.config.list.json';
import {Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedTableService} from 'packages/registrar/src/app/tables/tables.activated-table.service';
import {ErrorService, ModalService, ToastService, DIALOG_BUTTONS} from '@universis/common/public_api';
// tslint:disable-next-line:max-line-length
import {AdvancedSearchFormComponent} from 'packages/registrar/src/app/tables/components/advanced-search-form/advanced-search-form.component';

@Component({
  selector: 'app-classes-preview-students',
  templateUrl: './classes-students.component.html'
})
export class ClassesStudentsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration>CLASSES_STUDENTS_LIST_CONFIG;
  public recordsTotal: any;
  private dataSubscription: Subscription;
  @ViewChild('students') students: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  courseClassID: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _toastService: ToastService,
              private _context: AngularDataContext
  ) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      this._activatedTable.activeTable = this.students;

      this.students.query = this._context.model('StudentCourseClasses')
        .where('courseClass').equal(this.courseClassID)
        .expand('student($expand=department,person,studyProgram,semester,studentStatus,inscriptionYear,inscriptionPeriod)')
        .prepare();

      this.students.config = AdvancedTableConfiguration.cast(CLASSES_STUDENTS_LIST_CONFIG);
      this.students.fetch();

      this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
        if (fragment && fragment === 'reload') {
          this.students.fetch(true);
        }
      });

      this.dataSubscription = this._activatedRoute.data.subscribe(data => {
        if (data.tableConfiguration) {
          this.students.config = data.tableConfiguration;
          this.students.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.ngOnInit();
        }
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.formComponent.formLoad.subscribe((res: any) => {
            Object.assign(res, {courseClass: this.courseClassID});
          });
          this.search.ngOnInit();
        }
      });
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  remove() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.courseClassID = params.id;
      if (this.students && this.students.selected && this.students.selected.length) {
        const items = this.students.selected.map(item => {
          return {
            courseClass: this.courseClassID,
            student: item.id
          };
        });
        return this._modalService.showWarningDialog(
          this._translateService.instant('Classes.RemoveStudentTitle'),
          this._translateService.instant('Classes.RemoveStudentMessage'),
          DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('StudentCourseClasses').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Classes.RemoveStudentsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Classes.RemoveStudentsMessage.one' : 'Classes.RemoveStudentsMessage.many')
                  , {value: items.length})
              );
              this.students.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });

      }
    });
  }
}

import {
  ChangeDetectorRef,
  Component,
  ElementRef, EventEmitter,
  Inject,
  InjectionToken,
  Injector,
  Input, OnDestroy,
  OnInit, Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import 'datatables';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {TableConfiguration} from './advanced-table.interfaces';
import {DataServiceQueryParams} from '@themost/client/src/common';
import {AdvancedColumnFormatter} from './advanced-table.formatters';
import {ClientDataQueryable, TextUtils} from '@themost/client';
import {AdvancedFilterValueProvider} from './advanced-filter-value-provider.service';
import {Subscription} from 'rxjs';
import cloneDeep = require('lodash/cloneDeep');
import {TemplatePipe} from '@universis/common';
import {IAdvancedTableFormatters} from './advanced-table.formatters.interface';

declare var $: any;

export interface AdvancedTableDataResult {
  recordsTotal: number;
  recordsFiltered: number;
  data: Array<any>;
}

class TableTranslationChangeDetector extends ChangeDetectorRef {
  checkNoChanges(): void {
  }

  detach(): void {
  }

  detectChanges(): void {
  }

  markForCheck(): void {
  }

  reattach(): void {
  }

}

export let COLUMN_FORMATTERS = new InjectionToken('column.formatters');

class ColumnFormatter extends AdvancedColumnFormatter {
  constructor(protected injector: Injector) {
    super();
  }

  public class: string;
  public className: string;
  public defaultContent: string;
  public formatString: string;
  public formatter: string;
  public hidden: boolean;
  public name: string;
  public order: string;
  public property: string;
  public sortable: boolean;
  public title: string;

  render(data: any, type: any, row: any, meta: any) {
    //
  }
}

function forceCast<T>(input: any): T {
  return input;
}

export class AdvancedTableConfiguration {
  static cast(input: any, clone?: boolean): TableConfiguration {
    if (clone) {
      return forceCast<TableConfiguration>(cloneDeep(input));
    }
    return forceCast<TableConfiguration>(input);
  }
}

function getRandomIdentifier() {
  return (
    Number(String(Math.random()).slice(2))
  ).toString(36);
}

@Component({
  selector: 'app-advanced-table',
  template: `
    <table #table class="dataTable">
    </table>
    <div #emptyTable class="d-none">
      <div>
        <div class="mt-4">
          <div class="icon-circle bg-gray-300 border-gray-100"></div>
        </div>
        <div class="mt-4">
          <h4 class="font-3xl text-dark" [translate]="'Tables.EmptyTableTitle'"></h4>
          <p class="font-lg text-dark" [translate]="'Tables.EmptyTableMessage'"></p>
        </div>
      </div>
    </div>
  `,
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./advanced-table.component.scss']
})
export class AdvancedTableComponent implements OnInit, OnDestroy {


  private _query: ClientDataQueryable;
  private _config: TableConfiguration;
  private _firstLoad = true;
  private _showFooter = true;
  private _showPaging = false;
  private _id = getRandomIdentifier();
  public smartSelect = false;

  @ViewChild('table') table: ElementRef;
  @ViewChild('emptyTable') emptyTable: ElementRef;
  @Input() height = 800;
  @Input() showActions = true;
  @Input() autoLoad = true;
  @Output('load') load: EventEmitter<AdvancedTableDataResult> = new EventEmitter<AdvancedTableDataResult>();
  @Output('init') init: EventEmitter<any> = new EventEmitter<any>();
  @Input('reload') reload: EventEmitter<any> = new EventEmitter<any>();
  @Output('select') select: EventEmitter<any> = new EventEmitter<any>();
  @Output('selectedItems') selectedItems: EventEmitter<any> = new EventEmitter<any>();

  @Input() scrollable = true;
  @Input() serverSide = true;
  @Input() lengthMenu = [50, 100, 200, 500];
  @Input() pageLength = 25;

  @Input() scrollY = 800;
  @Input() scrollX = false;
  /**
   * Enables row selection. The defaulr value is false
   */
  @Input() selectable = false;
  /**
   * Enables or disable multiple selection. The default values is true.
   */
  @Input() multipleSelect = true;
  /**
   * Enables custom selection and hides select column. The default value is  false
   */
  @Input() customSelect = false;
  @Input() selected = [];
  public unselected = [];

  @Input('config')
  public set config(value: TableConfiguration) {
    this._config = value;
  }

  public get config() {
    return this._config;
  }

  @Input('showFooter')
  public set showFooter(value: boolean) {
    this._showFooter = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = (<HTMLDivElement>this._element.nativeElement)
        .querySelector('.dataTables_info');
      if (element == null) {
        return;
      }
      if (value) {
        element.classList.remove('d-none');
      } else {
        element.classList.add('d-none');
      }
    }
  }

  public get showFooter() {
    return this._showFooter;
  }

  @Input('showPaging')
  public set showPaging(value: boolean) {
    this._showPaging = value;
    // show or hide footer
    if (this._element && this._element.nativeElement) {
      const element = (<HTMLDivElement>this._element.nativeElement)
        .querySelector('.dataTables_paginate');
      if (element == null) {
        return;
      }
      if (value) {
        $(element).show();
      } else {
        $(element).hide();
      }
    }
  }

  public get showPaging() {
    return this._showPaging;
  }

  @Input('query')
  public set query(value: ClientDataQueryable) {
    this._query = value;
  }

  public get query() {
    return this._query;
  }

  public readonly translator: TranslatePipe;
  public dataTable: any;
  dtOptions: any;
  private lastQueryParams: DataServiceQueryParams;
  private reloadSubscription: Subscription;

  constructor(private _context: AngularDataContext,
              @Inject(COLUMN_FORMATTERS) private _columnFormatters: IAdvancedTableFormatters,
              private _activatedRoute: ActivatedRoute,
              private _translateService: TranslateService,
              private _injector: Injector,
              private _advancedFilterValueProvider: AdvancedFilterValueProvider,
              private _element: ElementRef,
              private _template: TemplatePipe) {
    // initialize translate pipe
    this.translator = new TranslatePipe(this._translateService, new TableTranslationChangeDetector());
    $.fn.dataTable.ext.classes.sPageButton = 'btn';
    $.fn.dataTable.ext.classes.sPageButtonActive = 'btn btn-light';
    this.reloadSubscription = this.reload.subscribe(data => {
      this.fetch();
    });
  }

  // simple search
  search(searchText) {
    this.query = this.query || this._context.model(this.config.model).asQueryable();
    if (searchText && this.config.searchExpression) {
      // validate search text in double quotes
      if (/^"(.*?)"$/.test(searchText)) {
        this.query.setParam('$filter',
          this._template.transform(this.config.searchExpression, {
            text: searchText.replace(/^"|"$/g, '')
          }));
      } else {
        // try to split words
        const words = searchText.split(' ');
        // join words to a single filter
        const filter = words.map(word => {
          return this._template.transform(this.config.searchExpression, {
            text: word
          });
        }).join(' and ');
        // set filter
        this.query.setParam('$filter', filter);
      }
    } else {
      // clear filter
      this.query.setParam('$filter', null);
    }
    this.dataTable.draw();
  }

  fetch(clearSelected?: boolean) {
    // if data table is null
    if (this.dataTable == null) {
      // initialize data table
      return this.ngOnInit();
    }
    // clear selection if parameter is not declared or it's true
    if (clearSelected == null || clearSelected === true) {
      // set smart select to false
      this.smartSelect = false;
      // clear selected items
      if (this.selected.length) {
        this.selected.splice(0, this.selected.length);
      }
      // clear unselected items
      if (this.unselected.length) {
        this.unselected.splice(0, this.unselected.length);
      }
    }
    // otherwise load data only
    this.dataTable.draw();
  }

  destroy() {
    if (this.dataTable) {
      this.dataTable.destroy();
      this._firstLoad = true;
      $(this.table.nativeElement).empty();
      this.dataTable = null;
      // clear selected items
      if (this.selected.length) {
        this.selected.splice(0, this.selected.length);
      }
      // clear unselected items
      if (this.unselected.length) {
        this.unselected.splice(0, this.unselected.length);
      }
      // set smart select to false
      this.smartSelect = false;
    }
  }

  /**
   * Selects the specified data row
   */
  selectRow(row: any, silent?: boolean) {
    if (row) {
      // get row
      const rowNode = row.node();
      // if multiple select is false
      if (this.multipleSelect === false) {
        if (this.customSelect === false) {
          // enumerate row
          this.dataTable.rows().nodes().each((node: any) => {
            if (node !== rowNode) {
              $(node).find(':checkbox').prop('checked', false);
            }
          });
        } else {
          this.dataTable.rows('.selected').nodes().each((node: any) => {
            if (node !== rowNode) {
              $(node).removeClass('selected');
            }
          });
        }
        // clear selected
        this.selected.splice(0, this.selected.length);
      }
      // if custom select mode is off
      if (this.customSelect === false) {
        $(rowNode).find(':checkbox').prop('checked', true);
      } else {
        // otherwise add class
        $(row.node()).addClass('selected');
      }
      // get row data
      const key = this.config.columns[0].property || this.config.columns[0].name;
      const data = row.data();
      // search selected items
      let findIndex = this.selected.findIndex((x) => {
        return x[key] === data[key];
      });
      if (findIndex >= 0) {
        // remove previously selected row
        this.selected.splice(findIndex, 1);
      }
      this.selected.push(data);
      // validate unselected data
      findIndex = this.unselected.findIndex((x) => {
        return x[key] === data[key];
      });
      if (findIndex >= 0) {
        // remove previously selected row
        this.unselected.splice(findIndex, 1);
      }
      if (silent) {
        return;
      }
      this.selectedItems.emit(this.selected);
    }
  }

  /**
   * Unselects the specified data row if it's selected
   */
  unSelectRow(row: any, silent?: boolean) {
    if (row) {
      // if custom select mode is off
      if (this.customSelect === false) {
        $(row.node()).find(':checkbox').prop('checked', false);
      } else {
        // otherwise add class
        $(row.node()).removeClass('selected');
      }
      // get first column key
      const key = this.config.columns[0].property || this.config.columns[0].name;
      const data = row.data();
      const id = data[key];
      let findIndex = this.selected.findIndex(x => {
        return x[key] === id;
      });
      if (findIndex >= 0) {
        this.selected.splice(findIndex, 1);
        if (this.smartSelect) {
          // add this row to unselected rows
          findIndex = this.unselected.findIndex(x => {
            return x[key] === id;
          });
          if (findIndex < 0) {
            this.unselected.push(data);
          }
        }
        if (silent) {
          return;
        }
        this.selectedItems.emit(this.selected);
      }
    }
  }

  selectAny() {
    if (this.dataTable && this.selectable) {
      // set smart select flag
      this.smartSelect = true;
      // select any row
      this.dataTable.rows().every((index: any) => {
        const row = this.dataTable.row( index );
        this.selectRow(row, true);
      });
      // emit selection event
      this.selectedItems.emit(this.selected);
    }
  }

  selectNone() {
    if (this.dataTable && this.selectable) {
      // set smart select flag
      this.smartSelect = false;
      this.unselected.splice(0, this.unselected.length);
      // select any row
      this.dataTable.rows().every((index: any) => {
        const row = this.dataTable.row( index );
        this.unSelectRow(row, true);
      });
      this.selected.splice(0, this.selected.length);
      // emit selection event
      this.selectedItems.emit(this.selected);
    }
  }

  toggleSelectRow(row: any) {
    if (row) {
      const data = row.data();
      // get first column key
      const key = this.config.columns[0].property || this.config.columns[0].name;
      // get id
      const id = data[key];
      // find if row is selected
      const findIndex = this.selected.find(x => {
        return x[key] === id;
      });
      // if row is selected
      if (findIndex >= 0) {
        // unselect
        this.unSelectRow(row);
      } else {
        this.selectRow(row);
      }
    }
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Datatables column renderer for select checkbox
   * @param data
   * @param type
   * @param row
   * @param meta
   * @private
   */
  private _renderSelect(data, type, row, meta) {
    // get column
    const column = meta.settings.aoColumns[meta.col];
    // render template
    return this._template.transform(`<input class="checkbox checkbox-theme s-row" type="checkbox"
 id="s-${this._id}-${meta.row}" data-id="${data}" /><label for="s-${this._id}-${meta.row}">&nbsp;</label>`, row)
  }

  ngOnInit() {
    const self = this;
    if (this.dataTable) {
      this.destroy();
    }
    if (this.config) {
      // parse properties
      if (Object.prototype.hasOwnProperty.call(this.config, 'selectable')) {
        this.selectable = this.config.selectable;
      }
      if (Object.prototype.hasOwnProperty.call(this.config, 'multipleSelect')) {
        this.multipleSelect = this.config.multipleSelect;
      }
      // prepare configuration for columns
      const tableColumns = this.config.columns
        // filter action link column
        .filter(column => {
          if (column.formatter && column.formatter === 'ActionLinkFormatter' && this.showActions === false) {
            return false;
          }
          return true;
        }).map(column => {
          // convert column to advance table column
          return Object.assign(new ColumnFormatter(this._injector), column);
        }).map(column => {
          const columnDefinition = {
            data: column.property || column.name,
            defaultContent: column.hasOwnProperty('defaultContent') ? column.defaultContent : '',
            title: this.translator.transform(column.title),
            sortable: column.hasOwnProperty('sortable') ? column.sortable : true,
            visible: column.hasOwnProperty('hidden') ? !column.hidden : true,
            name: column.name
          };

          if (column.className) {
            Object.assign(columnDefinition, {
              className: column.className
            });
          } else {
            Object.assign(columnDefinition, {
              className: ''
            });
          }

          if (column.formatters) {
            Object.assign(columnDefinition, {
              render: function (data, type, row, meta) {
                let result = data;
                column.formatters.forEach(columnFormatter => {
                  const formatter = self._columnFormatters[columnFormatter.formatter]
                  if (formatter && typeof formatter.render === 'function') {
                    column.formatString = columnFormatter.formatString;
                    column.formatOptions = columnFormatter.formatOptions;
                  }
                  result = formatter.render.bind(column)(result, type, row, meta);
                });
                delete column.formatString;
                delete column.formatOptions;
                return result;
              }
            });
          }
          if (column.formatter) {
            const formatter = self._columnFormatters[column.formatter]
            if (formatter && typeof formatter.render === 'function') {
              Object.assign(columnDefinition, {
                render: formatter.render.bind(column)
              });
            }
          }
          return columnDefinition;
        });
      // get table element
      const tableElement = $(this.table.nativeElement);
      // allow selection
      if (this.selectable) {
        // get first column (as row primary key)
        // todo::allow row primary key configuration
        const firstColumn = this.config.columns[0];
        // add select column
        const selectColumnDefinition = {
          className: 'text-center pl-4 pr-0',
          data: firstColumn.property || firstColumn.name,
          defaultContent: '',
          title: '',
          sortable: false,
          visible: !this.customSelect,
          name: firstColumn.name,
        };
        // assign renderer
        Object.assign(selectColumnDefinition, {
          render: this._renderSelect.bind(this)
        });
        // and finally add column
        tableColumns.unshift(selectColumnDefinition);
        // user interface additions
        // handle page change event (a trick for firing draw.dt events)
        tableElement
          // for each page
          .on('page.dt', ($event) => {
            const info = this.dataTable.page.info();
          })
          // handle draw events
          .on('draw.dt', () => {

            if (this.customSelect) {
              // enable row selection event
              tableElement.find('tbody').on('click', 'tr', function () {
                // select row
                const row = self.dataTable.row(this);
                // add row
                self.select.emit(row);
              });
            }
            tableElement.find('input.s-row').on('click', this.onSelectionChange.bind(this));
            // make selection on current page
            const key = this.config.columns[0].property;
            if (this.selected.length > 0) {
              // enumerate selected items
              this.selected.forEach(item => {
                // find row identifier
                const id = item[key];
                // if identifier is defined
                if (id != null) {
                  // try to find check element with this identifier in attribute data-id
                  tableElement.find(`input[data-id='${id}']`).prop('checked', true);
                }
              });
            }
            if (this.smartSelect) {
              // select rows that are not selected
              // and do not included in unselected collection
              tableElement.find('input[data-id]').each((index, element) => {
                // get data id
                if (this.unselected.findIndex((x) => {
                  // tslint:disable-next-line:triple-equals
                  return x[key] == element.attr('data-id');
                }) < 0) {
                  this.selectRow(this.dataTable.row(index));
                }
              });
            } else if (this.selected.length === 0) {
              tableElement.find('input[data-id]').prop('checked', false);
            }
          });
      }
      // initialize data table
      const settings = {
        // set length menu
        lengthMenu: [5, 10, 50, 100, 200, 500],
        // hide length menu
        lengthChange: false,
        // enable search but hide search box (css)
        searching: true,
        // enable data processing
        processing: true,
        // enable getting server side data
        serverSide: this.serverSide,
        // enumerate table columns
        columns: tableColumns,
        order: [],
        // set scroll y
        scrollY: this.scrollY,
        // set scroll x
        scrollX: this.scrollX,
        dom: 'Bfrtip',
        buttons: [
          {
            extend: 'excel',
            filename: this.config.title,
            className: 'd-none',
            exportOptions: {
              columns: 'th:not(:first-child)'
            }
          }
        ],
        // define server data callback
        fnServerData: function (sSource, aoData, fnCallback, oSettings) {
          // if method is invoked for the first time
          if (self._firstLoad) {
            // disabled first load flag
            self._firstLoad = false;
            // if auto load is disabled
            if (self.autoLoad === false) {
              const emptyDataSet = {
                recordsTotal: 0,
                recordsFiltered: 0,
                data: []
              };
              self._resetEmptyTable();
              self.load.emit(emptyDataSet);
              // return empty data
              return fnCallback(emptyDataSet);
            }
          }
          // get activated route params
          const queryParams = self._activatedRoute.snapshot.queryParams;
          // get columns
          const columns = aoData[1].value;
          // get order expression
          const order = aoData[2].value;
          // get skip records param
          const skip = aoData[3].value;
          // get page size param
          const top = aoData[4].value;
          // get search value
          const searchText = aoData[5].value;
          // get data queryable
          const q = self._query instanceof ClientDataQueryable ? self._query : self._context.model(self.config.model).asQueryable();
          // apply paging params
          if (top) {
            q.take(top).skip(skip);
          }
          // apply order params
          if (order && order.length) {
            // get order query expression
            const orderByStr = order.map(expr => {
              return tableColumns[expr.column].name + ' ' + expr.dir || 'asc';
            }).join(',');
            // set order
            q.setParam('$orderby', orderByStr);
          }
          // configure $select parameter
          const select = self.config.columns.filter( column => {
            if ( column.virtual === true ) {
              return false;
            }
            return true;
          }).map(column => {
            if (column.property) {
              return column.name + ' as ' + column.property;
            } else {
              return column.name;
            }
          });
          q.select.apply(q, select);
          // configure $filter
          if (queryParams.$filter && queryParams.$filter.length) {
            // set route filter
            const qParams = q.getParams();
            q.setParam('$filter', null);
            const expressions = [ queryParams.$filter ];
            if (qParams.$filter) { expressions.push('(' + qParams.$filter + ')' ); }
            q.filter(expressions.join(' and '));
          }
          // check if query parameters contain filter
          if (queryParams.$expand && queryParams.$expand.length) {
            // set route $expand
            q.setParam('$expand', queryParams.$expand);
          }

          if (!q.getParams()['$orderby'] && self.config && self.config.defaults && self.config.defaults.orderBy) {
            q.setParam('$orderby', self.config.defaults.orderBy);
          }
          // append default filter
          if (self.config && self.config.defaults && self.config.defaults.filter) {
            return self._advancedFilterValueProvider.asyncBuildFilter(self.config.defaults.filter).then(defaultFilterString => {
              if (defaultFilterString) {
                const qParams = q.getParams();
                q.setParam('$filter', null);
                const expressions = [ defaultFilterString ];
                if (qParams.$filter) { expressions.push('(' + qParams.$filter + ')' ); }
                q.filter(expressions.join(' and '));
              }
              self.lastQueryParams = q.getParams();
              q.getList().then(items => {
                const dataSet = {
                  recordsTotal: items.total,
                  recordsFiltered: items.total,
                  data: items.value
                };
                if (items.total === 0) {
                  self._resetEmptyTable();
                }
                // emit load event
                self.load.emit(dataSet);
                // return data
                return fnCallback(dataSet);
              });
            });
          }
          self.lastQueryParams = q.getParams();
          q.getList().then(items => {
            const dataSet = {
              recordsTotal: items.total,
              recordsFiltered: items.total,
              data: items.value
            };
            if (items.total === 0) {
              self._resetEmptyTable();
            }
            // emit load event
            self.load.emit(dataSet);
            // return data
            return fnCallback(dataSet);
          });
        },
        language: Object.assign(self.translator.transform('Tables.DataTable'),
          {emptyTable: this.emptyTable.nativeElement.innerHTML})
      };

      if (this.scrollable) {
        // set continuous scrolling
        Object.assign(settings, {
          scrollCollapse: false,
          scroller: {
            loadingIndicator: false,
            displayBuffer: 10
          },
        });
      } else {
        Object.assign(settings, {
          paging: true,
          pageLength: this.pageLength
        });
      }
      if (this.selectable) {

      }
      // handle init.dt event and emit local event
      this.dataTable = tableElement.DataTable(settings);
      tableElement.on('init.dt', () => {
        if (this.dataTable == null) {
          return;
        }
        // restore default empty table html
        // (due to ng async binding)
        this.init.emit(this.dataTable);
      });

      // show or hide footer
      if (!this.showFooter) {
        (<HTMLDivElement>this._element.nativeElement)
          .querySelector('.dataTables_info').classList.add('d-none');
      }
      // show or hide pager
      if (this.showPaging) {
        this.showPaging = true;
      }
    }
  }

  get lastQuery(): ClientDataQueryable {
    if (this.config == null) {
      return null;
    }
    if (this.lastQueryParams == null) {
      return null;
    }
    const q = this._context.model(this.config.model).asQueryable();
    Object.keys(this.lastQueryParams).forEach( (key) => {
      q.setParam(key, this.lastQueryParams[key]);
    });
    // return query
    return q;
  }

  /**
   * Fetches and updates a single row
   * @param find - Any object which is being converted to a query
   */
  fetchOne(find: any) {
    if (find == null) {
      return;
    }
    if (this.dataTable == null) {
     return;
    }
    if (this.lastQueryParams == null) {
      return;
    }
    // get rows
    const rows = Array.from(this.dataTable.rows().data());
    if (rows && rows.length) {
      // find result
      const findKeys = Object.keys(find);
      if (findKeys.length === 0) {
        // do nothing
        return;
      }
      // find row index by searching each property
      const rowIndex = rows.findIndex((row: any) => {
        return findKeys.map( (key) => {
          return row[key] === find[key];
        }).filter( (res) => {
          return res === false;
        }).length === 0;
      });
      // if row has been found
      if (rowIndex >= 0) {
        const query = this._context.model(this.config.model).asQueryable();
        // select attributes
        query.setParam('$select', this.lastQueryParams.$select);
        // apply filter for row
        findKeys.forEach( (key) => {
          const column = this.config.columns.find( (col) => {
            return col.property === key || col.name === key;
          });
          if (column) {
            // get property or name
            const property = column.property || column.name;
            query.and(property).equal(find[key]);
          } else {
            query.and(key).equal(find[key]);
          }
        });
        // and finally get item
        return query.getItem().then( (item) => {
          // if item exists
          const row = this.dataTable.row(rowIndex);
          const checked = $(row.node()).find(':checkbox').prop('checked');
          if (item) {
            // redraw row
            row.data(item);
            $(row.node()).find(':checkbox').on('click', this.onSelectionChange.bind(this));
            // if row is already selected
            if (checked) {
              this.selectRow(row, true);
            }
          }
          return Promise.resolve();
        });
      }
    }
    return Promise.resolve();
  }

  ngOnDestroy(): void {
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
  }

  /**
   * Handle selection change
   * @param $event
   */
  onSelectionChange($event) {
    const input = <HTMLInputElement>$event.target;
    // get row from id
    const rowIndex = parseInt(/-(\d+)$/g.exec(input.id)[1], 10);
    // get row
    const row = this.dataTable.row(rowIndex).data();
    // get row identifier
    if (row) {
      const key = this.config.columns[0].property;
      const id = row[key];
      let findItemIndex = this.selected.findIndex(item => {
        return item[key] === id;
      });
      if (input.checked) {
        if (findItemIndex < 0) {
          // add row to selected rows
          this.selected.push(row);
        }
      } else {
        if (findItemIndex >= 0) {
          // remove selected item
          this.selected.splice(findItemIndex, 1);
          if (this.smartSelect) {
            // add row to unselected rows
            findItemIndex = this.unselected.findIndex(item => {
              return item[key] === id;
            });
            if (findItemIndex < 0) {
              this.unselected.push(row);
            }
          }
        }
      }
    }
  }

  private _resetEmptyTable() {
    // this operation is important
    // due to ng data binding
    // empty table template has no translated text during ngOnInit()
    if (this.dataTable && this.dataTable.context[0]) {
      this.dataTable.context[0].oLanguage.sEmptyTable = this.emptyTable.nativeElement.innerHTML;
    }
  }

  export() {
    const element: HTMLElement = document.querySelector('.dt-buttons .buttons-excel') as HTMLElement;
    element.click();
  }


  handleColumns(columnsToShow) {
    const table = this.dataTable;
    this.config.columns.slice(1).forEach((column, i) => {
      if (!column.hidden) {
        if (columnsToShow.includes(column.name)) {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(true);
        } else {
          // noinspection TypeScriptValidateJSTypes
          table.column(column.name + ':name').visible(false);
        }
      }
    });
  }

  getConfig() {
    return this.config;
  }
}

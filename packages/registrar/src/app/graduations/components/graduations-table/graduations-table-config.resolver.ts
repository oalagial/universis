import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class GraduationsTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./graduations-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./graduations-table.config.list.json`);
        });
    }
}

export class GraduationsTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./graduations-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./graduations-table.search.list.json`);
            });
    }
}

export class GraduationsDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./default-graduations-table.config.json`);
    }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { AdvancedTableConfiguration, AdvancedTableComponent, AdvancedTableDataResult } from 'packages/registrar/src/app/tables/components/advanced-table/advanced-table.component';
import { AdvancedSearchFormComponent } from 'packages/registrar/src/app/tables/components/advanced-search-form/advanced-search-form.component';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import * as GRADUATIONS_GRADUATES_LIST_CONFIG from './graduations-graduates.config.json';
import * as GRADUATIONS_GRADUATES_SEARCH_CONFIG from './graduations-graduates.search.json';

@Component({
  selector: 'app-graduations-graduates',
  templateUrl: './graduations-graduates.component.html'
})
export class GraduationsGraduatesComponent implements OnInit {

  public recordsTotal: any;
  @ViewChild('graduates') graduates: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  graduationEventId: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this.graduates.query = this._context.model('GraduationRequestActions')
      .where('graduationEvent').equal(this.graduationEventId)
      .expand('student($expand=studyProgram,department,person,semester,requeststatus,inscriptionYear,inscriptionPeriod,studentStatus)')
      .prepare();

    this.graduates.config = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_LIST_CONFIG);
    this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_GRADUATES_SEARCH_CONFIG);
    this.graduates.ngOnInit();
    this.search.ngOnInit();

    }

    onDataLoad(data: AdvancedTableDataResult) {
      this.recordsTotal = data.recordsTotal;
    }

}

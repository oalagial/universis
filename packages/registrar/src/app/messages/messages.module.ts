import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { MessagesSharedModule } from './messages.shared';
import { SharedModule } from 'packages/common/src/public_api';
import { FormsModule } from '@angular/forms';
import { SendMessageToInstructorComponent } from './components/send-message-to-instructor/send-message-to-instructor.component';


@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    MessagesSharedModule,
    SharedModule,
    FormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  exports: [
    SendMessageToInstructorComponent
  ],
  declarations: [SendMessageToInstructorComponent]
})
export class MessagesModule { }

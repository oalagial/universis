import {Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {from, Observable, Subscription} from 'rxjs';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../tables/components/advanced-table/advanced-table.component';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';
import {ErrorService, LoadingService, ModalService, UserActivityService} from '@universis/common';
import { TranslateService } from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';
import {AdvancedRowActionComponent} from '../../../tables/components/advanced-row-action/advanced-row-action.component';
import {ClientDataQueryable} from '@themost/client';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';


@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styles: []
})
export class StudentsTableComponent implements OnInit, OnDestroy {
  private selectedItems = [];
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;

  @Input() department: any;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();
  public recordsTotal: any;
  private fromProgram: any;
  private fromSpecialty: any;


  constructor(
    private _activatedRoute: ActivatedRoute,
    private _userActivityService: UserActivityService,
    private _translateService: TranslateService,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _modalService: ModalService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _activeDepartmentService: ActiveDepartmentService
  ) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.formComponent.formLoad.subscribe((res: any) => {
          Object.assign(res, { department: this._activatedRoute.snapshot.data.department.id });
        });
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = AdvancedTableConfiguration.cast( data.tableConfiguration);
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(true);
        });
      }
      if (this.table.config) {
        return this._userActivityService.setItem({
          category: this._translateService.instant('Students.StudentTitle'),
          description: this._translateService.instant(this.table.config.title),
          url: window.location.hash.substring(1),
          dateCreated: new Date()
        });
      }
    });
  }

  /**
   * Change study program for selected students
   */
  async changeStudyProgram() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same specialty and study program
      const fromProgram =  [...new Set(items.map(item => item.studyProgram))];
      const fromSpecialty =  [...new Set(items.map(item => item.studyProgramSpecialty))];
      if (fromProgram.length !== 1 || fromSpecialty.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        this.fromSpecialty = fromSpecialty;
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'currentSpecialty': this.fromSpecialty && this.fromSpecialty.length ? this.fromSpecialty[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeProgram' : null,
          modalTitle: 'Students.ChangeProgramAction.Title',
          description: 'Students.ChangeProgramAction.Description',
          errorMessage: 'Students.ChangeProgramAction.CompletedWithErrors',
          additionalMessage:  this._translateService.instant('Students.ChangeProgramAction.NoItems'),
          refresh: this.refreshAction,
          execute: this.executeChangeProgramAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Change study program for selected students
   */
  async changeSpecialty() {
    try {
      this._loadingService.showLoading();
      let message = this._translateService.instant('Students.ChangeSpecialtyAction.NoItems');
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      // change study program only for students that attend same study program
      const fromProgram =  [...new Set(items.map(item => item.studyProgram))];
        if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
          // check if study program has specialties
          const specialties = await this._context.model('StudyProgramSpecialties').select('count(id) as total')
            .where('studyProgram').equal(this.fromProgram[0])
            .and('specialty').notEqual(-1).getItem();
          if (specialties && specialties.total === 0) {
            this.selectedItems = [];
            message = this._translateService.instant('Students.ChangeSpecialtyAction.NoSpecialties');
          }

      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
            'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/changeSpecialty' : null,
          modalTitle: 'Students.ChangeSpecialtyAction.Title',
          description: 'Students.ChangeSpecialtyAction.Description',
          errorMessage: 'Students.ChangeSpecialtyAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeChangeSpecialtyAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Add study program groups
   */
  async addProgramGroup() {
    try {
      this._loadingService.showLoading();
      let items = await this.getSelectedItems();
      items =  items.filter( (item) => {
        return item.status === 'active';
      });
      let message = this._translateService.instant('Students.AddProgramGroupAction.NoItems');
      // create exams can be made only for course classes that belong to same year and period
      const fromProgram =  [...new Set(items.map(item => item.studyProgram))];
      if (fromProgram.length !== 1) {
        this.selectedItems = [];
      } else {
        this.selectedItems = items;
        this.fromProgram = fromProgram;
        // check if study program has groups
        const groups = await this._context.model('ProgramGroups').select('count(id) as total').where('program').equal(this.fromProgram[0])
          .and('groupType/alternateName').equal('simple').and('parentGroup').notEqual(null).getItem();
        if (groups && groups.total === 0) {
          this.selectedItems = [];
          message = this._translateService.instant('Students.AddProgramGroupAction.NoGroups');
        }
      }
      this._loadingService.hideLoading();
      // get active department
      const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          data: {
            'currentProgram': this.fromProgram && this.fromProgram.length ? this.fromProgram[0] : null,
             'department': activeDepartment.id
          },
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'Students/addStudentGroup' : null,
          modalTitle: 'Students.AddProgramGroupAction.Title',
          description: 'Students.AddProgramGroupAction.Description',
          errorMessage: 'Students.AddProgramGroupAction.CompletedWithErrors',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeAddProgramGroup()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes add program group for students
   */
  executeAddProgramGroup() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      // check if student already has student group
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.group == null || data.group === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load program group
            const group = await this._context.model('ProgramGroups').where('id').equal(data.group).getItem();
            data.object = item.id;
            data.groups = [group];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const exists = await this._context.model('StudentProgramGroups')
              .where('student').equal(item.id).and('programGroup').equal(data.group).select('id').getItem();
            if (!exists) {
              const action = await this._context.model(`UpdateStudentGroupActions`).save(data);
              if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
                result.success += 1;
              } else {
                result.errors += 1;
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes update of specialty for students
   */
  executeChangeSpecialtyAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.specialty === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            data.specialty = data.specialty;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`UpdateSpecialtyActions`).save(data);
            if (action && action.actionStatus && action.actionStatus.alternateName === 'CompletedActionStatus') {
              result.success += 1;
              try {
                await this.table.fetchOne({
                  id: item.id
                });
              } catch (err) {
                //
              }
            } else {
              result.errors += 1;
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }
  /**
   * Executes change program action for students
   */
  executeChangeProgramAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      if (data.specialty == null || data.studyProgram == null || data.specialty === '' || data.studyProgram === '') {
        this.selectedItems = [];
        result.errors = result.total;
        return observer.next(result);
      }
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            data.object = item.id;
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`UpdateProgramActions`).save(data);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async calculateSemester() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only active students
      this.selectedItems = items.filter( (item) => {
        return item.status === 'active';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Students.CalculateSemesterAction.Title',
          description: 'Students.CalculateSemesterAction.Description',
          refresh: this.refreshAction,
          execute: this.executeCalculateSemester()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Calculates semester for selected students
   */
  executeCalculateSemester() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const studentSemester = await this._context.model(`Students/${item.id}/currentSemester`).getItems();
            if (studentSemester && item.semester !== studentSemester.value) {
              item.semester = studentSemester.value;
              await this._context.model('Students').save(item);
              result.success += 1;
            }
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'studentStatus/alternateName as status', 'semester',
            'studyProgram/id as studyProgram', 'studyProgramSpecialty/id as studyProgramSpecialty'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.studentStatus,
              semester: item.semester,
              studyProgram : item.studyProgramId,
              studyProgramSpecialty: item.studyProgramSpecialtyId
            };
          });
        }
      }
    }
    return items;
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }
}

import {Injectable} from '@angular/core';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class StudentTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./students-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./students-table.config.list.json`);
        });
    }
}

export class StudentTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./students-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./students-table.search.list.json`);
            });
    }
}

export class StudentDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./students-table.config.list.json`);
    }
}

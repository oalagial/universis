import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-theses',
  templateUrl: './students-theses.component.html'
})
export class StudentsThesesComponent implements OnInit, OnDestroy  {
  public model: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('StudentTheses')
        .where('student').equal(params.id)
        .expand('thesis($expand=startYear,instructor,status,type)')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-suspensions',
  templateUrl: './students-overview-suspensions.component.html',
  styles: [`
    .suspension-row:last-child {
      border-bottom: 0 !important;
    }
  `]
})
export class StudentsOverviewSuspensionsComponent implements OnInit, OnDestroy  {

  public lastSuspensions: any;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
      this.lastSuspensions = await this._context.model('StudentSuspensions')
        .where('student').equal(this.studentId)
        .orderByDescending('suspensionDate')
        .getItems();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

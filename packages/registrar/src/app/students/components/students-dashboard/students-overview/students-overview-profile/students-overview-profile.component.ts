import { Component, Input, OnChanges, OnDestroy, OnInit, SimpleChanges } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-students-overview-profile',
  templateUrl: './students-overview-profile.component.html',
  styleUrls: ['./students-overview-profile.component.scss']
})
export class StudentsOverviewProfileComponent implements OnInit, OnDestroy, OnChanges {

  public student;
  @Input() studentId: number;

  //  Value to indicate whether the message send form should be visible or not (This is passed to child component)
  public showMessageForm = false;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('person($expand=gender), department, studyProgram')
        .getItem()
        .then((value) => { this.student = value; });
    }
  }

  ngOnInit() { }

  /*
   *  This Functions toggles Message Send Form Visibility
   */
  enableMessages() {
    this.showMessageForm = !this.showMessageForm;
  }

  /*
   *  This functions is used to receive message sent status from child component
   */
  onsuccesfulSend(succesful: boolean) {
    //  Then toggles messge form visibility accordingly
    this.showMessageForm = !succesful;
  }

  ngOnDestroy(): void { }
}

import {Component, OnInit, Input, OnDestroy, SimpleChanges} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-overview-removal',
  templateUrl: './students-overview-removal.component.html'
})
export class StudentsOverviewRemovalComponent implements OnInit, OnDestroy  {

  public student;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.studentId) {
      if (changes.studentId.currentValue == null) {
        this.student = null;
        return;
      }
      this._context.model('Students')
        .where('id').equal(changes.studentId.currentValue)
        .expand('person($expand=gender), department, studyProgram')
        .getItem()
        .then((value) => { this.student = value; });
    }
  }


  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.student = await this._context.model('Students')
        .where('id').equal(this.studentId)
        .expand('person($expand=gender), department, studyProgram')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute, Router} from '@angular/router';
import { AdvancedSearchFormComponent } from '../../../../../tables/components/advanced-search-form/advanced-search-form.component';
import * as SEARCH_CONFIG from './student-courses-table.search.list.json';
import { Pipe, PipeTransform } from '@angular/core';
import {ErrorService, LoadingService, ModalService, TemplatePipe} from '@universis/common';
import {Observable, Subscription} from 'rxjs';
import {AdvancedFilterValueProvider} from '../../../../../tables/components/advanced-table/advanced-filter-value-provider.service';
import {EditCoursesComponent} from '../../../../../study-programs/components/preview/edit-courses/edit-courses.component';

@Component({
  selector: 'app-students-courses-by-group',
  templateUrl: './students-courses-by-group.component.html',
  styleUrls: ['./students-courses-by-group.component.scss']
})
export class StudentsCoursesByGroupComponent implements OnInit , OnDestroy {
  public model: any;
  public data: any;
  public groups = [];
  public studentId;
  private subscription: Subscription;
  private student: any;

  @ViewChild('search') search: AdvancedSearchFormComponent;
  collapsed = true;

  public groupTypes = [
    {
      key: 'semester',
      prop: 'semester.alternateName',
      name: 'Students.Semester'
    },
    {
      key: 'type',
      prop: 'courseType.id',
      name: 'Students.CourseType'
    }
  ];
  public selectedGroupType = this.groupTypes[0];
  private searchExpression = '(student eq ${student} and (indexof(course/name, \'${text}\') ge 0 or indexof(course/displayCode, \'${text}\') ge 0 or ' +
    'indexof(course/courseArea/name, \'${text}\') ge 0 or indexof(course/courseCategory/name, \'${text}\') ge 0 or indexof(courseType/name, \'${text}\') ge 0))';
  private allStudentCourses: any;

  public defaultGradeScale: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _router: Router,
              private _errorService: ErrorService,
              private _advancedFilter: AdvancedFilterValueProvider) {
  }

  async  ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.student = await this._context.model('Students')
        .where('id')
        .equal(params.id)
        .expand('studyProgram($expand=gradeScale)')
        .getItem();

      this.defaultGradeScale = this.student && this.student.studyProgram && this.student.studyProgram.gradeScale;

      this.model = await this._context.model('StudentCourses')
        .where('student').equal(params.id)
        .expand('programGroup, course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor)), studyProgramSpecialty')
        .take(-1)
        .getItems();

      this.allStudentCourses = this.model;

      this.selectedGroupType = this.groupTypes[0];
      this.search.form = SEARCH_CONFIG;
      this.search.ngOnInit();

      this.parseCourseParts();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  parseCourseParts() {
    // get parent courses for orphaned children after search
    const parents = this.allStudentCourses.filter(el => {
        return !this.model.map(x => x.course.id).includes(el.course.id);
    }).filter(el => {
        return this.model.map(x => x.parentCourse).includes(el.course.id);
    });

    // remove course parts and add parents
    this.data = this.model.filter(studentCourse => {
      return studentCourse.courseStructureType && studentCourse.courseStructureType.id !== 8;
    }).concat(parents);

    // add courseParts as courses to each parent course
    this.data.forEach( studentCourse => {
      // get course parts
      if (studentCourse.courseStructureType && studentCourse.courseStructureType.id === 4) {
        studentCourse.courses = this.model.filter(course => {
          return course.parentCourse === studentCourse.course.id;
        }).sort((a, b) => {
          return a.course.displayCode < b.course.displayCode ? -1 : 1;
        });
      }
    });
  }

  async onSearchKeyDown(event: any) {
    const q = this._context.model('StudentCourses').asQueryable();

    const searchText = (<HTMLInputElement>event.target);
    if (searchText && event.keyCode === 13) {
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        if (searchText && searchText.value && this.searchExpression) {
          // validate search text in double quotes
          if (/^"(.*?)"$/.test(searchText.value)) {
            q.setParam('$filter',
              this._template.transform(this.searchExpression, {
                student: this.studentId,
                text: searchText.value.replace(/^"|"$/g, '')
              }));
          } else {
            // try to split words
            const words = searchText.value.split(' ');
            // join words to a single filter
            const filter = words.map(word => {
              return this._template.transform(this.searchExpression, {
                student:  this.studentId,
                text: word
              });
            }).join(' and ');
            // set filter
            q.setParam('$filter', filter);
          }
        } else {
          // use only student filter
          q.setParam('$filter', 'student eq ' + this.studentId);
        }
        this.model = await q.expand('course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor))')
          .take(-1).getItems();

        this.parseCourseParts();
      });
    }
  }

  async advancedSearch(event?: any) {
    if (Array.isArray(this.search.form.criteria)) {
      const expressions = [];
      const filter = this.search.filter;
      const values = this.search.formComponent.formio.data;
      this.subscription = this._activatedRoute.params.subscribe(async (params) => {
        this.studentId = params.id;
        this.search.form.criteria.forEach((x) => {
          if (Object.prototype.hasOwnProperty.call(filter, x.name)) {
            // const nameFilter = this.convertToString(filter[x.name]);
            if (filter[x.name] !== 'undefined') {
              expressions.push(this._template.transform(x.filter, Object.assign({
                value: filter[x.name]
              }, values)));
            }
          }
        });
        expressions.push('(student eq ' +  this.studentId  + ')');

        // create client query
        const q = this._context.model('StudentCourses').asQueryable();
        q.setParam('filter', expressions.join(' and '));

        this.model = await q.expand('course($expand=instructor,gradeScale),gradeExam($expand=instructors($expand=instructor))')
          .take(-1).getItems();

        this.parseCourseParts();
      });
    }
  }

  async editCourse(course: any) {
    this._loadingService.showLoading();
    this._modalService.openModalComponent(EditCoursesComponent, {
      class: 'modal-xl',
      keyboard: true,
      ignoreBackdropClick: false,
      initialState: {
        items: [course],
        modalTitle: 'Students.EditCourse',
        formEditName: 'StudentCourses/course-attributes/edit',
        toastHeader: 'Students.EditStudentCourse',
        courseProperties: {
          courseTitle: course.courseTitle,
          notes: course.notes,
          calculateGrade:  course.calculateGrade !== 0,
          calculateUnits: course.calculateUnits !== 0,
          specialty: course.studyProgramSpecialty,
          programGroup: course.programGroup,
          student: this.student
        },
        execute:  (() => {
          return new Observable((observer) => {
            this._loadingService.showLoading();
            // get add courses component
            const component = <EditCoursesComponent>this._modalService.modalRef.content;
            // fix data structure
            const submissionData = component.items.map(x => {
              return {
                calculateGrade: x.calculateGrade ? 1 : 0,
                calculateUnits: x.calculateUnits ? 1 : 0,
                coefficient: x.coefficient,
                course: x.course.id,
                courseTitle: x.courseTitle,
                courseType: x.courseType,
                ects: x.ects,
                notes: x.notes,
                semester: x.semester,
                student: x.student,
                units: x.units,
                specialty: x.specialty.specialty,
                programGroup: x.programGroup.id ? x.programGroup.id : null
              };
            });
            // and submit
            this._context.model('StudentCourses').save(submissionData).then(async() => {
              await this.ngOnInit();
              this._loadingService.hideLoading();
              observer.next();
            }).catch((err) => {
              this._loadingService.hideLoading();
              observer.error(err);
            });
          });
        })()
      }
    });
    this._loadingService.hideLoading();
  }
}

@Pipe({
    name: 'gradeAverage'
})
export class GradeAveragePipe implements PipeTransform {
  transform(items: any[]): any {
    const courses = items.filter(studentCourse => {
      return studentCourse.courseStructureType.id !== 8 && studentCourse.isPassed && studentCourse.calculateUnits
             && studentCourse.course.gradeScale.scaleType !== 3 && studentCourse.grade !== null;
    });
    if (courses && courses.length > 0) {
      return courses.reduce((a, b) => a + b.grade, 0) / courses.length;
    } else {
      return 0;
    }
  }
}

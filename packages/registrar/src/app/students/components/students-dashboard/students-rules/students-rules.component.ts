import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-students-rules',
  templateUrl: './students-rules.component.html'
})

export class StudentsRulesComponent implements OnInit, OnDestroy  {

  private subscription: Subscription;
  public student: any;

  constructor(private _context: AngularDataContext,  private _activatedRoute: ActivatedRoute, private _loadingService: LoadingService) { }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.student = params.id;
    });
  }




  ngOnDestroy(): void {
    if (this.subscription) {
      this.student = null;
      this.subscription.unsubscribe();
    }
  }
}

import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {LoadingService} from '@universis/common';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-internships',
  templateUrl: './students-internships.component.html',
  styleUrls: ['./students-internships.component.scss']
})
export class StudentsInternshipsComponent implements OnInit, OnDestroy {

  public model: any;
  private subscription: Subscription;

  constructor(private _element: ElementRef,
              private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _loadingService: LoadingService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this._loadingService.showLoading();
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Internships')
        .where('student').equal(params.id)
        .expand('status,internshipPeriod,department,student($expand=person)')
        .getItems();
      this._loadingService.hideLoading();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

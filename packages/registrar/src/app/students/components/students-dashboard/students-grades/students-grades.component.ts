import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {AdvancedTableComponent, AdvancedTableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.component';
import * as STUDENTS_GRADES_LIST_CONFIG from './students-grades.config.json';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-students-grades',
  templateUrl: './students-grades.component.html'
})
export class StudentsGradesComponent implements OnInit, OnDestroy  {
  public readonly config = STUDENTS_GRADES_LIST_CONFIG;
  private subscription: Subscription;

  @ViewChild('grades') grades: AdvancedTableComponent;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.grades.query = this._context.model('StudentCourses')
        .where('student').equal(params.id)
        .expand('course')
        .prepare();
      this.grades.config = AdvancedTableConfiguration.cast(STUDENTS_GRADES_LIST_CONFIG);
      this.grades.fetch();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}

import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as THESES_INSTRUCTORS_LIST_CONFIG from './theses-preview-instructos.config.list.json'
import { Subscription } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-instructors',
  templateUrl: './theses-dashboard-grades.component.html',
})
export class ThesesDashboardGradesComponent implements OnInit {

  private subscription: Subscription;
  public model: any;

  @Input() editable = false;
  @Input() editMode = false;
  
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.model = await this._context.model('Theses')
        .where('id').equal(params.id)
        .expand('instructor,type,status,students($expand=student($expand=department,person,studentStatus))')
        .getItem();

      const grades = await  this._context.model('StudentThesisResults')
        .where('thesis').equal(params.id)
        .expand('instructor')
        .getItems();

      this.model.students.forEach( student => {
        student.results = grades.filter( x => {
         return x.student === student.student.id;
        });
      });
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}

export const REGISTRAR_APP_LOCATIONS = [
  {
    'privilege': 'Location',
    'target': {
        'url': '^/auth/'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'target': {
        'url': '^/error'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'account': {
        'name': 'Administrators'
    },
    'target': {
        'url': '^/'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'account': {
      'name': 'Library'
    },
    'target': {
      'url': '^/guest'
    },
    'mask': 1
  },
  {
    'privilege': 'Location',
    'account': {
      'name': 'Library'
    },
    'target': {
      'url': '^/dashboard/?$'
    },
    'mask': 0,
    'redirectTo': '/guest/'
  },
  {
    'privilege': 'Location',
    'account': {
      'name': 'Registrar'
    },
    'target': {
      'url': '^/'
    },
    'mask': 1
  }
];

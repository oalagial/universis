import { NgModule, OnInit, OnDestroy } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsRoutingModule } from './settings.routing';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SharedModule } from '@universis/common';
import { TablesModule } from '../tables/tables.module';
import { MostModule, AngularDataContext } from '@themost/angular';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ListComponent } from './components/list/list.component';
import { SettingsSharedModule } from '../settings-shared/settings-shared.module';
import { SectionsComponent } from './components/sections/sections.component';
import { SettingsService, SettingsSection } from '../settings-shared/services/settings.service';
import sourceAt = require('lodash/get');
import template = require('lodash/template');
import { ActivatedUser } from 'packages/common/src/auth/services/activated-user.service';
import { Subscription } from 'rxjs';
import { RouterModalModule } from '@universis/common/routing';
import { NgArrayPipesModule } from 'ngx-pipes';
import {TemplatePipe} from '@universis/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    TablesModule,
    MostModule,
    TranslateModule,
    SettingsRoutingModule,
    SettingsSharedModule,
    RouterModalModule,
    NgArrayPipesModule
  ],
  declarations: [
    HomeComponent,
    ListComponent,
    SectionsComponent
  ],
  providers: [TemplatePipe]
})
export class SettingsModule implements OnDestroy {

  private subscription: Subscription;

  constructor(private _context: AngularDataContext,
    private _settings: SettingsService,
    private _translateService: TranslateService,
    private _activatedUser: ActivatedUser,
    private _template: TemplatePipe) {
      this.subscription = this._activatedUser.user.subscribe( user => {
        if (user) {
          this.initialize();
        }
      });
  }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  private async initialize() {
    const metadata = await this._context.getMetadata();
    const translations = this._translateService.instant('Settings.Lists');
    const items = metadata.EntityType.filter( x => {
      return x.ImplementsType === 'Enumeration';
    }).map( x =>  {
      // get long description fallback
      // use it if translation is missing
      const description = sourceAt(translations, `${x.Name}.Description`) || x.Name;
      const longDescriptionFallback = this._template.transform(translations.LongDescriptionFallback,
        {Description: description},
        {interpolate:  /{{([\s\S]+?)}}/g } );
      const entitySet = metadata.EntityContainer.EntitySet.find( y => {
        return y.EntityType === x.Name;
      });
      // use lodash.get in order to allow translation fallback
      return <SettingsSection> {
        category: 'Lists',
        name: x.Name,
        description: description,
        longDescription: sourceAt(translations, `${x.Name}.LongDescription`) || longDescriptionFallback,
        url: `/settings/lists/${entitySet.Name}`
      };
    });
    this._settings.addSection.apply(this._settings, items);
  }
}

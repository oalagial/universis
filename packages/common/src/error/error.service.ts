import { Injectable, Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';

export declare interface ShowErrorModalOptions {
  continueLink?: string;
  buttonText?: string;
  iconClass?: string;
}

@Component( {
  selector: 'universis-error-modal',
  template: `
    <div class="modal-header text-center">
      <button type="button" class="close pull-right" aria-label="Close" (click)="hide()">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body mt-0 mb-0 text-center">
      <div class="text-center">
            <div class="icon-circle icon-circle-danger">
                <i class="fa fa-times"></i>
            </div>
            <div class="font-2xl font-weight-bold mt-2">
              {{title}}
            </div>
            <p class="mt-2">
             {{message}}
            </p>
        </div>
    </div>
    <div class="modal-footer">
      <button [lang]="language" type="button" (click)="hide()"
              class="btn btn-gray-100 btn-ok text-uppercase" [translate]="buttonText"></button>
    </div>
  `, styles: [`
  .btn-ok {
    font-size: 16px;
  }
  `]
   })

  export class ErrorModalComponent implements OnInit {

    public code;
    public language;
    @Input() continueLink = '/';
    @Input() message: string;
    @Input() title: string;
    @Input() iconClass = 'far fa-frown';
    @Input() buttonText = 'Error.Continue';
    @Input() error: any;

    constructor ( private _translateService: TranslateService,
                  public bsModalRef: BsModalRef,
                  private _router: Router ) {
      this.language = this._translateService.currentLang;
    }

  hide() {
      this.bsModalRef.hide();
      if (this.continueLink == null) {
        return Promise.resolve();
      }
      if (this.continueLink === '.') {
        return Promise.resolve();
      }
      return this._router.navigate([this.continueLink]);
    }

    ngOnInit() {

      // get last error
      const error = this.error;
      // check error.code property
      if (error && typeof error.code === 'string') {
        this.code = error.code;
      } else if (error && typeof (error.status || error.statusCode) === 'number') {
        this.code = `E${error.status || error.statusCode}`;
      } else {
        this.code = 'E500';
      }
      if (error && typeof error.continue === 'string') {
        this.continueLink = error.continue;
      }

      this._translateService.get(this.code).subscribe((translation) => {
        if (translation) {
          this.title = translation.title;
          this.message = translation.message;
        } else {
          this._translateService.get('E500').subscribe((result) => {
            this.title = result.title;
            this.message = result.message;
          });
        }
      });
    }
  }


@Injectable()
export class ErrorService {

  private _lastError: any;

  constructor(private _router: Router, private _modalService: BsModalService) {
    //
  }

  /**
   * @param {*} error
   * @returns {Promise<boolean>}
   */
  navigateToError(error) {
    this.setLastError(error);
    // if error is an instance of HttpErrorResponse
    if (error instanceof HttpErrorResponse) {
      if (error && error.error && error.error.statusCode) {
        return this._router.navigate(['/error', error.error.statusCode]);
      }
      // navigate to specific error e.g. /error/401
      // this will allow application to override specific error pages and show custom errors
      return this._router.navigate(['/error', error.status]);
    }
    // otherwise show default error component
    return this._router.navigate(['/error']);
  }

  showError(error: any, options?: ShowErrorModalOptions) {
    const initialState = Object.assign({
      error: error
    }, options);
    this._modalService.show(ErrorModalComponent, {initialState});
  }

  /**
   * Sets last application error
   * @param {*=} err
   * @return ErrorService
   */
  setLastError(err?: any) {
    this._lastError = err;
    return this;
  }

  /**
   * Gets last application error
   * @return {any}
   */
  getLastError() {
    return this._lastError;
  }

}

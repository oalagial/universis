import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {HttpClient} from '@angular/common/http';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import { EdmSchema } from '@themost/client';



export interface FormPreProcessor {
  parse(form: any): void;
}

export interface AsyncFormPreProcessor {
  parseAsync(form: any): Promise<void>;
}

export class QueryParamsPreProcessor {
  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {}
  parse(form: any) {
    form.queryParams = this._activatedRoute.snapshot.queryParams;
  }

}

export class ServiceUrlPreProcessor {
  constructor(private _context: AngularDataContext) {}
  parse(form: any) {
    const headers = this._context.getService().getHeaders();
    const serviceHeaders = Object.keys(headers).map(key => {
      return {
        key: key,
        value: headers[key]
      };
    });
    this.parseComponents(form.components, serviceHeaders);
    return form;
  }

  private parseComponents(components: Array<any>, headers: any) {
    if (Array.isArray(components) === false) {
      return;
    }
    components.forEach( component => {
      if (component.data && component.data.url) {
        if (/^(http:|https:)/ig.test(component.data.url) === false) {
          component.data.url = this._context.getService().resolve(component.data.url);
          component.data.headers = component.data.headers || [];
          component.data.headers.push.apply(component.data.headers, headers);
        }
      }
      if (component.components) {
        return this.parseComponents(component.components, headers);
      }
      if (component.columns) {
        component.columns.forEach( column => {
          if (column.components) {
            this.parseComponents(column.components, headers);
          }
        });
      }
    });
  }

}

@Injectable()
export class AdvancedFormsService {

  constructor(private _context: AngularDataContext,
              private _http: HttpClient,
              private _activatedRoute: ActivatedRoute,
              private translateService: TranslateService) {
    //
  }

  async loadForm(name: string): Promise<any> {
      return await new Promise((resolve, reject) => {
        this._http.get(`assets/forms/${name}.json`).subscribe( result => {
            // tslint:disable-next-line: no-use-before-declare
            new ServiceUrlPreProcessor(this._context).parse(result);
            new QueryParamsPreProcessor(this._activatedRoute, this._context).parse(result);
            // new TranslatePreProcessor(this.translateService).parse(result);
            return resolve(result);
        }, error => {
          return reject(error);
        });
      });
  }
}

export class TranslatePreProcessor {
  constructor(private _translateService: TranslateService) {}
  parse(form: any) {
    this.parseComponents(form.components);
    return form;
  }

  private parseComponents(components: Array<any>) {
    if (Array.isArray(components) === false) {
      return;
    }
    components.forEach( component => {
      if (component.label) {
        const translation = this._translateService.instant(component.label);
        const isEqualType = typeof component.label === typeof translation;
        if(isEqualType){
          component.label = translation;
        }
      }
      if (component.placeholder) {
        component.placeholder = this._translateService.instant(component.placeholder);
      }
      if (component.components) {
        return this.parseComponents(component.components);
      }
      if (component.columns) {
        component.columns.forEach( column => {
          if (column.components) {
            this.parseComponents(column.components);
          }
        });
      }
      if (component.data && component.data.values && Array.isArray(component.data.values) ) {
          this.parseComponents(component.data.values);
      }
    });
  }
}
/**
 * Parses activated route data and assign them to a form which has a valid target model
 */
export class ActivatedRouteDataPreProcessor implements AsyncFormPreProcessor {
  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) {}
  async parseAsync(form: any) {
    // get schema
    const schema = await this._context.getMetadata();
    // if form.model exists do pre-processing
    if (form.model) {
      const data = {};
      // try to assign data
      this._assign(schema, form.model, data, this._activatedRoute.snapshot.data);
      // if form data does not exist
      if (form.data == null) {
        // set form data
        form.data = data;
      } else {
        // otherwise assign data
        Object.assign(form.data, data);
      }
      return form;
    }
  }

  private _assign(schema: EdmSchema, entitySet: string, dest: any, source: any) {
    const add = { };
    // find entity set based on model name
    const findEntitySet = schema.EntityContainer.EntitySet.find( x => {
      return x.Name === entitySet;
    });
    if (findEntitySet) {
      // find entity type
      const findEntityType = schema.EntityType.find( x => {
        return x.Name === findEntitySet.EntityType;
      });
      if (findEntityType) {
        const properties = findEntityType.Property.slice(0);
        const navigationProperties = findEntityType.NavigationProperty.slice(0);
        if (findEntityType.BaseType) {
          let baseType = schema.EntityType.find( x => {
            return x.Name === findEntityType.BaseType;
          });
          while (baseType) {
            properties.push.apply(properties, baseType.Property);
            navigationProperties.push.apply(navigationProperties, baseType.NavigationProperty);
            baseType = baseType.BaseType && schema.EntityType.find(x => {
              return x.Name === baseType.BaseType;
            });
          }
        }
        // enumerate params
        const  keys = Object.keys(source);
        for (let i = 0; i < keys.length; i++) {
          const key = keys[i];
          if (Object.prototype.hasOwnProperty.call(source, key) && source[key] != null) {
            const property = key.split('/');
            if (property.length === 1) {
              const testProperty = new RegExp(`^${property[0]}$`);
              // check if query param is a property of the specified model
              const findProperty = properties.find( x => {
                return testProperty.test(x.Name);
              });
              // and assign value
              if (findProperty) {
                Object.defineProperty(add, findProperty.Name, {
                  enumerable: true,
                  configurable: true,
                  writable: true,
                  value: source[key]
                });
              } else {
                // search navigation property
                const findNavigationProperty = navigationProperties.find( x => {
                  return testProperty.test(x.Name);
                });
                // if query param is a navigation property
                if (findNavigationProperty) {
                  // and set property
                  Object.defineProperty(add, findNavigationProperty.Name, {
                    enumerable: true,
                    configurable: true,
                    writable: true,
                    value: source[key]
                  });
                }
              }
            }
          }
        }
      }
      // finally assign values
      Object.assign(dest, add);
      return dest;
    }
  }

}

@Injectable()
export class AdvancedFormResolver implements Resolve<string> {
  constructor(private _translateService: TranslateService, private _forms: AdvancedFormsService) {}
  resolve(
      route: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
  ): Promise<any>|any {
    const action = route.data.action || route.params.action;
    const model = route.data.model || route.params.model;
    return this._forms.loadForm(`${model}/${action}`);
  }
}
